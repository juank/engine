

#version 330 core
out vec4 color;

in vec2 TexCoords;

uniform sampler2D texture_diffuse1;

uniform float hasTransparency;

void main()
{
    vec4 diffuseColor = texture(texture_diffuse1, TexCoords);
        if(hasTransparency > 0.5 && diffuseColor.a < 0.1) {
        discard;
    }
    color = diffuseColor;
}

