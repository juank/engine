#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 5) in ivec4 BoneIDs;
layout (location = 6) in vec4 Weights;

out VS_OUT {
  vec2 texCoord;
  vec3 normal;
  vec3 worldPosition;
  vec3 timNormal;
  vec4 worldPositionLightSpace;
} vs_out;

const int MAX_BONES = 100;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 lightSpaceMatrix;

uniform mat4 gBones[MAX_BONES];

void main()
{
    mat4 BoneTransform = gBones[BoneIDs[0]] * Weights[0];
    BoneTransform += gBones[BoneIDs[1]] * Weights[1];
    BoneTransform += gBones[BoneIDs[2]] * Weights[2];
    BoneTransform += gBones[BoneIDs[3]] * Weights[3];

    vec4 localPosition = BoneTransform * vec4(position, 1.0);
    vec4 localNormal = BoneTransform * vec4(normal, 0.0);

    vec4 worldPosition = modelMatrix * localPosition;

    vs_out.texCoord = texCoord;

    vs_out.normal = (modelMatrix * localNormal).xyz;
    vs_out.worldPosition = vec3(worldPosition);
    vs_out.timNormal = mat3(transpose(inverse(modelMatrix))) * normal; // TODO: enviar como uniform
    vs_out.worldPositionLightSpace = lightSpaceMatrix * vec4(vs_out.worldPosition, 1.0);

    gl_Position = projectionMatrix * viewMatrix * worldPosition;
}
