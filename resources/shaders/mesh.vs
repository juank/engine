#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;

out VS_OUT {
  vec2 texCoord;
  vec3 normal;
  vec3 worldPosition;
  vec3 timNormal;
  vec4 worldPositionLightSpace;
} vs_out;

const int MAX_BONES = 100;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 lightSpaceMatrix;


void main()
{

    vec4 localPosition = vec4(position, 1.0);
    vec4 localNormal = vec4(normal, 0.0);




    vec4 worldPosition = modelMatrix * localPosition;

    vs_out.texCoord = texCoord;

    vs_out.normal = (modelMatrix * localNormal).xyz;
    vs_out.worldPosition = vec3(worldPosition);
    vs_out.timNormal = mat3(transpose(inverse(modelMatrix))) * normal; // TODO: enviar como uniform
    vs_out.worldPositionLightSpace = lightSpaceMatrix * vec4(vs_out.worldPosition, 1.0);

    gl_Position = projectionMatrix * viewMatrix * worldPosition;
}
