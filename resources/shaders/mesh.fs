#version 330 core
struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    sampler2D texture_reflection1;
    sampler2D emission;
    float shininess;
};
struct DirectionalLight {
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
struct PointLight {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};
struct SpotLight {
    vec3 position;
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;

    float cutOff;
    float outerCutOff;
};


in VS_OUT {
  vec2 texCoord;
  vec3 normal;
  vec3 worldPosition;
  vec3 timNormal;
  vec4 worldPositionLightSpace;
} fs_in;

out vec4 color;

uniform DirectionalLight directionalLight;
uniform SpotLight spotLight;
#define NR_POINT_LIGHTS 4
uniform PointLight pointLights[NR_POINT_LIGHTS];

uniform vec3 viewPosition;
uniform Material material;

uniform float hasTransparency;
uniform float blend;

uniform samplerCube skybox;

uniform sampler2D depthTextureSampler;

vec3 calcDirectionalLight(DirectionalLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor, vec3 specularColor, float shadow);
vec3 calcPointLight(PointLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor, vec3 specularColor);
vec3 calcSpotLight(SpotLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor, vec3 specularColor);
vec3 calcReflection();
float calcShadow(vec3 uNormal);

void main()
{
    // draw depth buffer
    /*float depth = linearizeDepth(gl_FragCoord.z) / far; // divide by far for demonstration
    color = vec4(vec3(depth), 1.0f);
    return;*/



    // skybox refraction
    /*float ratio = 1.00 / 1.52;
    vec3 I = normalize(_worldPosition - viewPosition);
    vec3 R = refract(I, normalize(_timNormal), ratio);
    color = texture(skybox, R);
    return;*/

    vec4 diffuseColor = texture(material.texture_diffuse1, fs_in.texCoord);
		if(hasTransparency > 0.5 && diffuseColor.a < 0.1) {
        discard;
    }

    //JK diffuseColor += vec4(calcReflection(), diffuseColor.a);

    vec3 specularColor = texture(material.texture_specular1, fs_in.texCoord).xyz;

    // Properties
    vec3 uNormal = normalize(fs_in.normal);
    vec3 viewDirection = normalize(viewPosition - fs_in.worldPosition);

    float shadow = 0.0;//JK calcShadow(uNormal);

    // Phase 1: Directional lighting
    vec3 result = calcDirectionalLight(directionalLight, uNormal, viewDirection, diffuseColor.xyz, specularColor, shadow);
    // Phase 2: Point lights
    /*JK for(int i = 0; i < NR_POINT_LIGHTS; i++) {
        result += calcPointLight(pointLights[i], uNormal, viewDirection, diffuseColor.xyz, specularColor);
    }*/
    // Phase 3: Spot light
    //result += calcSpotLight(spotLight, uNormal, viewDirection, diffuseColor.xyz, specularColor);


    vec3 emissionLight = vec3(0.0, 0.0, 0.0);//texture(material.emission, _texCoord).xyz;

    color = vec4(emissionLight+result, blend > 0.5 ? diffuseColor.a : 1.0);

}

float calcShadow(vec3 uNormal) {
    // perform perspective divide
    vec3 projCoords = fs_in.worldPositionLightSpace.xyz / fs_in.worldPositionLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    //float closestDepth = texture(depthTextureSampler, projCoords.xy).r;
    // Get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // Check whether current frag pos is in shadow
    float bias = 0.0002;
    /*vec3 uLightDirection = normalize(-directionalLight.direction);
    float bias = max(0.05 * (1.0 - dot(uNormal, uLightDirection)), 0.005);*/
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(depthTextureSampler, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(depthTextureSampler, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;


    if(projCoords.z > 1.0) {
        shadow = 0.0;
    }
    return shadow;
}

vec3 calcReflection() {
    // skybox reflection
    vec3 I = normalize(fs_in.worldPosition - viewPosition);
    vec3 R = reflect(I, normalize(fs_in.timNormal));
    float reflect_intensity = texture(material.texture_reflection1, fs_in.texCoord).r;
    vec3 reflect_color;
    if(reflect_intensity > 0.1) // Only sample reflections when above a certain treshold
        reflect_color = texture(skybox, R).xyz * reflect_intensity;
    // Combine them
    return reflect_color;
}

vec3 calcDirectionalLight(DirectionalLight light, vec3 uNormal, vec3 viewDirection, vec3 diffuseColor, vec3 specularColor, float shadow)
{
    vec3 uLightDirection = normalize(-light.direction);
    // Diffuse shading
    float diffuseImpact = max(dot(uNormal, uLightDirection), 0.0);
    // Specular shading: pong
    /*vec3 reflectDirection = reflect(-uLightDirection, uNormal);
    float dampedFactor = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);*/
    // Specular shading: blinn pong
    vec3 halfwayDirection = normalize(uLightDirection + viewDirection);
    float dampedFactor = pow(max(dot(uNormal, halfwayDirection), 0.0), material.shininess);
    // Combine results
    vec3 ambientLight = light.ambient * diffuseColor;
    vec3 diffuseLight = light.diffuse * diffuseImpact * diffuseColor;
    vec3 specularLight = light.specular * (dampedFactor * specularColor);
    return ambientLight + ( (1 - shadow) * (diffuseLight + specularLight) );
}

vec3 calcPointLight(PointLight light, vec3 uNormal, vec3 viewDirection, vec3 diffuseColor, vec3 specularColor)
{
    vec3 uLightDirection = normalize(light.position - fs_in.worldPosition);
    // Diffuse shading
    float diffuseImpact = max(dot(uNormal, uLightDirection), 0.0);
    // Specular shading: pong
    /*vec3 reflectDirection = reflect(-uLightDirection, uNormal);
    float dampedFactor = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);*/
    // Specular shading: blinn pong
    vec3 halfwayDirection = normalize(uLightDirection + viewDirection);
    float dampedFactor = pow(max(dot(uNormal, halfwayDirection), 0.0), material.shininess);
    // Attenuation
    float distance = length(light.position - fs_in.worldPosition);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // Combine results
    vec3 ambientLight = light.ambient * diffuseColor;
    vec3 diffuseLight = light.diffuse * diffuseImpact * diffuseColor;
    vec3 specularLight = light.specular * (dampedFactor * specularColor);

    ambientLight *= attenuation;
    diffuseLight *= attenuation;
    specularLight *= attenuation;
    return (ambientLight + diffuseLight + specularLight);
}

vec3 calcSpotLight(SpotLight light, vec3 uNormal, vec3 viewDirection, vec3 diffuseColor, vec3 specularColor)
{
    vec3 uLightDirection = normalize(light.position - fs_in.worldPosition);
    // Diffuse shading
    float diffuseImpact = max(dot(uNormal, uLightDirection), 0.0);
    /// Specular shading: pong
    /*vec3 reflectDirection = reflect(-uLightDirection, uNormal);
    float dampedFactor = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);*/
    // Specular shading: blinn pong
    vec3 halfwayDirection = normalize(uLightDirection + viewDirection);
    float dampedFactor = pow(max(dot(uNormal, halfwayDirection), 0.0), material.shininess);
    // Attenuation
    float distance = length(light.position - fs_in.worldPosition);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // Combine results
    vec3 ambientLight = light.ambient * diffuseColor;
    vec3 diffuseLight = light.diffuse * diffuseImpact * diffuseColor;
    vec3 specularLight = light.specular * (dampedFactor * specularColor);

    ambientLight *= attenuation;
    diffuseLight *= attenuation;
    specularLight *= attenuation;

    float theta = dot(uLightDirection, normalize(-light.direction));
    float epsilon   = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    // We'll leave ambient unaffected so we always have a little light.
    diffuseLight  *= intensity;
    specularLight *= intensity;
    return (ambientLight + diffuseLight + specularLight);
}
