#version 330 core

struct DirectionalLight {
    vec3 direction;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
struct PointLight {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};
struct SpotLight {
    vec3 position;
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;

    float cutOff;
    float outerCutOff;
};

in VS_OUT {
  vec2 texCoord;
  vec3 normal;
  vec3 worldPosition;
  vec4 worldPositionLightSpace;
  vec4 texColor;
} fs_in;

out vec4 color;

uniform sampler2D depthTextureSampler;

uniform DirectionalLight directionalLight;
uniform SpotLight spotLight;
#define NR_POINT_LIGHTS 4
uniform PointLight pointLights[NR_POINT_LIGHTS];

uniform vec3 viewPosition;

vec3 calcDirectionalLight(DirectionalLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor, float shadow);
vec3 calcPointLight(PointLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor);
vec3 calcSpotLight(SpotLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor);
float calcShadow(vec3 uNormal);

float linearizeDepth(float depth);
float near = 1.0;
float far  = 100.0;

void main()
{
    // draw depth buffer
    /*float depth = linearizeDepth(gl_FragCoord.z) / far; // divide by far for demonstration
    color = vec4(vec3(depth), 1.0f);
    return;*/

    vec2 tiledCoord = fs_in.texCoord * 40;

    //vec4 texColor = vec4(0.145, 0.215, 0.102, 1.0);
    vec4 texColor = fs_in.texColor;

    // Properties
    vec3 uNormal = normalize(fs_in.normal);
    vec3 viewDirection = normalize(viewPosition - fs_in.worldPosition);

    float shadow = 0.0; //JKcalcShadow(uNormal);

    // Phase 1: Directional lighting
    vec3 result = calcDirectionalLight(directionalLight, uNormal, viewDirection, texColor.xyz, shadow);
    // Phase 2: Point lights
    /*for(int i = 0; i < NR_POINT_LIGHTS; i++) {
        result += calcPointLight(pointLights[i], uNormal, viewDirection, texColor.xyz);
    }
    // Phase 3: Spot light
    result += calcSpotLight(spotLight, uNormal, viewDirection, texColor.xyz);
*/
    //vec3 emissionLight = texture(material.emission, _texCoord).xyz;

    //color = vec4(emissionLight+result, 1.0);
    color = vec4(result, 1.0);

}

float calcShadow(vec3 uNormal) {
    // perform perspective divide
    vec3 projCoords = fs_in.worldPositionLightSpace.xyz / fs_in.worldPositionLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    //float closestDepth = texture(depthTextureSampler, projCoords.xy).r;
    // Get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // Check whether current frag pos is in shadow
    float bias = 0;//.005;
    //vec3 uLightDirection = normalize(-directionalLight.direction);
    //float bias = max(0.05 * (1.0 - dot(uNormal, uLightDirection)), 0.005);
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(depthTextureSampler, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(depthTextureSampler, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    if(projCoords.z > 1.0) {
        shadow = 0.0;
    }
    return shadow;




}

vec3 calcDirectionalLight(DirectionalLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor, float shadow)
{
    vec3 uLightDirection = normalize(-light.direction);
    // Diffuse shading
    float diffuseImpact = max(dot(uNormal, uLightDirection), 0.0);
    // Specular shading
    //vec3 reflectDirection = reflect(-uLightDirection, uNormal);
    //float dampedFactor = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    // Combine results
    vec3 ambientLight = light.ambient * texColor;
    vec3 diffuseLight = light.diffuse * diffuseImpact * texColor;
    //vec3 specularLight = light.specular * (dampedFactor * texture(material.specular, _texCoord).xyz);
    //return (ambientLight + diffuseLight + specularLight);
    return ambientLight + ( (1.0-shadow) * diffuseLight );
}

vec3 calcPointLight(PointLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor)
{
    vec3 uLightDirection = normalize(light.position - fs_in.worldPosition);
    // Diffuse shading
    float diffuseImpact = max(dot(uNormal, uLightDirection), 0.0);
    // Specular shading
    //vec3 reflectDirection = reflect(-uLightDirection, uNormal);
    //float dampedFactor = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    // Attenuation
    float distance = length(light.position - fs_in.worldPosition);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // Combine results
    vec3 ambientLight = light.ambient * texColor;
    vec3 diffuseLight = light.diffuse * diffuseImpact * texColor;
    //vec3 specularLight = light.specular * (dampedFactor * texture(material.specular, _texCoord).xyz);

    ambientLight *= attenuation;
    diffuseLight *= attenuation;
    //specularLight *= attenuation;
    //return (ambientLight + diffuseLight + specularLight);
    return (ambientLight + diffuseLight);
}

vec3 calcSpotLight(SpotLight light, vec3 uNormal, vec3 viewDirection, vec3 texColor)
{
    vec3 uLightDirection = normalize(light.position - fs_in.worldPosition);
    // Diffuse shading
    float diffuseImpact = max(dot(uNormal, uLightDirection), 0.0);
    // Specular shading
    //vec3 reflectDirection = reflect(-uLightDirection, uNormal);
    //float dampedFactor = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);
    // Attenuation
    float distance = length(light.position - fs_in.worldPosition);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // Combine results
    vec3 ambientLight = light.ambient * texColor;
    vec3 diffuseLight = light.diffuse * diffuseImpact * texColor;
    //vec3 specularLight = light.specular * (dampedFactor * texture(material.specular, _texCoord).xyz);

    ambientLight *= attenuation;
    diffuseLight *= attenuation;
    //specularLight *= attenuation;

    float theta = dot(uLightDirection, normalize(-light.direction));
    float epsilon   = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    // We'll leave ambient unaffected so we always have a little light.
    diffuseLight  *= intensity;
    //specularLight *= intensity;
    //return (ambientLight + diffuseLight + specularLight);
    return (ambientLight + diffuseLight);
}

float linearizeDepth(float depth) {
    float z = depth * 2.0 - 1.0; // Back to NDC
    return (2.0 * near * far) / (far + near - z * (far - near));
}
