#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;

out VS_OUT {
  vec2 texCoord;
  vec3 normal;
  vec3 worldPosition;
  vec4 worldPositionLightSpace;
  vec4 texColor;
} vs_out;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat4 lightSpaceMatrix;

void main()
{
    vec4 worldPosition = modelMatrix * vec4(position, 1.0f);

    if(worldPosition.y < 20) {
        vs_out.texColor = vec4(0.0f, 0.0f, 1.0f, 1.0f);
    } else if(worldPosition.y < 80) {
        vs_out.texColor = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    } else {
        vs_out.texColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    }

    vs_out.texCoord = texCoord;
    vs_out.normal = (modelMatrix * vec4(normal, 0.0f)).xyz;
    vs_out.worldPosition = worldPosition.xyz;
    vs_out.worldPositionLightSpace = lightSpaceMatrix * vec4(vs_out.worldPosition, 1.0);

    gl_Position = projectionMatrix * viewMatrix * worldPosition;
}
