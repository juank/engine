This is my sandbox to learn about opengl and practice c++ programming.  
It's mainly based in this awesome site: https://learnopengl.com/  

[![](http://img.youtube.com/vi/N38GR0E2mIw/0.jpg)](http://www.youtube.com/watch?v=N38GR0E2mIw "Demo 1")

How to compile it:  
Install GCC:  
`sudo apt update`  
`sudo apt install build-essential`

Install OpenGL:  
`sudo apt install mesa-common-dev mesa-utils
`

Install GLFW3:  
`sudo apt install libglfw3-dev`

Install Assimp:  
`sudo apt install libassimp-dev assimp-utils`  

