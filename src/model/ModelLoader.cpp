#include "ModelLoader.h"
#include <assimp/Importer.hpp>

#include <assimp/postprocess.h>
#include <iostream>
#include "Mesh.h"

#include "../_lib/glm/gtc/type_ptr.hpp"
#include "../_lib/stb/stb_image.h"


ModelLoader::ModelLoader() {
}

// loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
Model ModelLoader::loadModel(std::string const &path, bool loadBones) {
    // read file via ASSIMP
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace | aiProcess_LimitBoneWeights );
    // check for errors
    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
    {
        std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
        return Model();
    }

    Model model(path, loadBones);

    // process ASSIMP's root node recursively
    processNode(model, scene->mRootNode, scene);

    return model;
}

void ModelLoader::setTexture(Model& model, const std::string& fileName, OpenGLLoader& loader) {
    for (auto &mesh : model.meshes) {
        GLuint tex = loader.loadTexture(model.directory + "/" + fileName);
        Texture texture;
        texture.id = TextureFromFile(fileName.c_str(), model.directory);
        texture.type = "texture_diffuse";
        //texture.path = str;
        mesh.textures.push_back(texture);
    }
}

void ModelLoader::processNode(Model& model, aiNode *node, const aiScene *scene)
{
    std::cout << "NODE: " << node->mName.C_Str() << std::endl;
    // process each mesh located at the current node
    for(unsigned int i = 0; i < node->mNumMeshes; i++)
    {
        // the node object only contains indices to index the actual objects in the scene.
        // the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        model.meshes.push_back(processMesh(model, mesh, scene));
    }
    // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
    for(unsigned int i = 0; i < node->mNumChildren; i++)
    {
        processNode(model, node->mChildren[i], scene);
    }
}

Mesh ModelLoader::processMesh(Model& model, const aiMesh* mesh, const aiScene* scene) {
    // data to fill
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;

    // Walk through each of the mesh's vertices
    for(unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        Vertex vertex;
        glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
        // positions
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;
        // normals
        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.Normal = vector;
        // texture coordinates
        if(mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
        {
            glm::vec2 vec;
            // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        }
        else
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);
        // tangent
        vector.x = mesh->mTangents[i].x;
        vector.y = mesh->mTangents[i].y;
        vector.z = mesh->mTangents[i].z;
        vertex.Tangent = vector;
        // bitangent
        vector.x = mesh->mBitangents[i].x;
        vector.y = mesh->mBitangents[i].y;
        vector.z = mesh->mBitangents[i].z;
        vertex.Bitangent = vector;
        vertices.push_back(vertex);
    }
    // now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
    for(unsigned int i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        // retrieve all indices of the face and store them in the indices vector
        for(unsigned int j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }
    // process materials
    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
    // we assume a convention for sampler names in the shaders. Each diffuse texture should be named
    // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
    // Same applies to other texture as the following list summarizes:
    // diffuse: texture_diffuseN
    // specular: texture_specularN
    // normal: texture_normalN

    // 1. diffuse maps
    std::vector<Texture> diffuseMaps = loadMaterialTextures(model, material, aiTextureType_DIFFUSE, "texture_diffuse");
    textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
    // 2. specular maps
    std::vector<Texture> specularMaps = loadMaterialTextures(model, material, aiTextureType_SPECULAR, "texture_specular");
    textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
    // 3. normal maps
    std::vector<Texture> normalMaps = loadMaterialTextures(model, material, aiTextureType_HEIGHT, "texture_normal");
    textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
    // 4. height maps
    std::vector<Texture> heightMaps = loadMaterialTextures(model, material, aiTextureType_AMBIENT, "texture_height");
    textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());


    std::vector<VertexBoneData> meshBones;

    if(model.loadBones) {
        meshBones.resize(vertices.size());
        loadBones(model, mesh, meshBones);
    }

    // return a mesh object created from the extracted mesh data
    return Mesh(vertices, indices, textures, meshBones);
}

std::vector<Texture> ModelLoader::loadMaterialTextures(Model& model, aiMaterial *mat, aiTextureType type, std::string typeName) {
    std::vector<Texture> textures;
    for(unsigned int i = 0; i < mat->GetTextureCount(type); i++)
    {
        aiString str;
        mat->GetTexture(type, i, &str);
        // check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
        bool skip = false;
        for(unsigned int j = 0; j < model.textures_loaded.size(); j++)
        {
            if(std::strcmp(model.textures_loaded[j].path.C_Str(), str.C_Str()) == 0)
            {
                textures.push_back(model.textures_loaded[j]);
                skip = true; // a texture with the same filepath has already been loaded, continue to next one. (optimization)
                break;
            }
        }
        if(!skip)
        {   // if texture hasn't been loaded already, load it
            std::cout << "loadtex: " << typeName << " " << str.C_Str() << std::endl;
            Texture texture;
            texture.id = TextureFromFile(str.C_Str(), model.directory);
            texture.type = typeName;
            texture.path = str;
            textures.push_back(texture);
            model.textures_loaded.push_back(texture);  // store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
        }
    }
    return textures;
}

void ModelLoader::loadBones(Model& model, const aiMesh* pMesh, std::vector<VertexBoneData>& meshBones) {
    for (uint i = 0 ; i < pMesh->mNumBones ; i++) {
        uint boneIndex = 0;
        std::string boneName(pMesh->mBones[i]->mName.data);
        boneName = boneName.substr(boneName.find("_") + 1);

        if (model.bones.find(boneName) == model.bones.end()) {
            // Allocate an index for a new bone
            std::cout << "ADD BONE: " << boneName << std::endl;
            boneIndex = model.bones.size();
            Model::BoneInfo boneInfo;
            model.boneInfos.push_back(boneInfo);
            model.boneInfos[boneIndex].boneOffset = glm::make_mat4(&pMesh->mBones[i]->mOffsetMatrix.a1);
            model.bones[boneName] = boneIndex;
        }
        else {
            boneIndex = model.bones[boneName];
        }


        //assert(pMesh->mBones[i]->mNumWeights <= NUM_BONES_PER_VEREX);
        for (uint j = 0 ; j < pMesh->mBones[i]->mNumWeights ; j++) {
            uint vertexID = /*m_Entries[MeshIndex].BaseVertex + */pMesh->mBones[i]->mWeights[j].mVertexId;
            GLfloat weight = pMesh->mBones[i]->mWeights[j].mWeight;

            //std::cout << "addBonde b:" << boneIndex << " v:" << vertexID << " w:" << weight << std::endl;
            meshBones[vertexID].addBoneData(boneIndex, weight);
        }
    }
}

unsigned int ModelLoader::TextureFromFile(const char *path, const std::string &directory, bool gamma) {
    std::string filename = std::string(path);
    filename = directory + '/' + filename;

    unsigned int textureID;
    glGenTextures(1, &textureID);

    stbi_set_flip_vertically_on_load(false);

    int width, height, nrComponents;
    unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}