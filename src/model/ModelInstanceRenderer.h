#pragma once

#include "../core/AbstractRenderer.h"
#include "Model.h"

class ModelInstanceRenderer: public AbstractRenderer {
    Model model;
    std::vector<glm::mat4> modelMatrices;

    glm::vec3 PITCH_VECTOR;
    glm::vec3 YAW_VECTOR;
    glm::vec3 ROLL_VECTOR;

public:
    ModelInstanceRenderer();

    void render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera);
    void bindTextures(Mesh& mesh);// TODO: duplicated on ModelRenderer
    //func depthRender()
    void prepareModel(Model& model);
    void addInstance(glm::mat4& modelMatrix);
};
