#include "ModelEntity.h"

ModelEntity::ModelEntity(Model model, glm::vec3 position, GLfloat scale, GLfloat pitch, GLfloat yaw, GLfloat roll) :
    model{model},
    position{position},
    scale{scale},
    pitch{pitch},
    yaw{yaw},
    roll{roll} {
    this->model = model; //todo
}