#include "ModelInstanceRenderer.h"
#include <sstream>
#include <iostream>

ModelInstanceRenderer::ModelInstanceRenderer():
    AbstractRenderer{"../resources/shaders/mesh_instance.vs", "../resources/shaders/mesh.fs"},
    PITCH_VECTOR{1.0f, 0.0, 0.0},
    YAW_VECTOR{0.0, 1.0, 0.0},
    ROLL_VECTOR{0.0, 0.0, 1.0} {
}

void ModelInstanceRenderer::render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera) {
    // Activate shader
    shader.use();

    bindCommon(viewMatrix, projectionMatrix, camera);

    //shader.bindTextureCubeMap("skybox", tex: skybox.texCubemap)
    //shader.bindTexture("depthTextureSampler", tex: Sun.texDepth)
    shader.bindUniform("material.shininess", 32.0f);

    shader.bindUniform("hasTransparency", model.hasTransparency ? 1.0f : 0.0f);
    //shader.bindUniform("blend", float: model.toBlend ? 1.0 : 0.0)

    /*if model.disableCulling {
        glDisable(GL_CULL_FACE)
    }*/

    for( auto& mesh : model.meshes) {
        bindTextures(mesh);

        glBindVertexArray(mesh.VAO);
        glDrawElementsInstanced(GL_TRIANGLES, GLsizei(mesh.indices.size()), GL_UNSIGNED_INT, nullptr, GLsizei(modelMatrices.size()));
        glBindVertexArray(0);
    }

    /*if model.disableCulling {
        glEnable(GL_CULL_FACE)
    }*/
}

void ModelInstanceRenderer::bindTextures(Mesh& mesh) {
    // bind appropriate textures
    unsigned int diffuseNr  = 1;
    unsigned int specularNr = 1;
    unsigned int normalNr   = 1;
    unsigned int heightNr   = 1;
    //std::cout << "ok0" <<  std::endl;
    for(unsigned int i = 0; i < mesh.textures.size(); i++)
    {
        //std::cout << "ok1" << std::endl;
        GLenum err = glGetError();
        //std::cout << "ok2" << std::endl;
        //std::cout << "error:" << err << std::endl;
        glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
        //std::cout << "ok3" << std::endl;
        // retrieve texture number (the N in diffuse_textureN)
        std::stringstream ss;
        std::string number;
        std::string name = mesh.textures[i].type;
        if(name == "texture_diffuse")
            ss << diffuseNr++; // transfer unsigned int to stream
        else if(name == "texture_specular")
            ss << specularNr++; // transfer unsigned int to stream
        else if(name == "texture_normal")
            ss << normalNr++; // transfer unsigned int to stream
        else if(name == "texture_height")
            ss << heightNr++; // transfer unsigned int to stream
        number = ss.str();
        // now set the sampler to the correct texture unit
        glUniform1i(glGetUniformLocation(shader.ID, ("material."+name+number).c_str()), i);
        // and finally bind the texture
        glBindTexture(GL_TEXTURE_2D, mesh.textures[i].id);
    }
}

/*func depthRender() {
    if model.disableCulling {
        glDisable(GL_CULL_FACE)
    }

    for mesh in model.meshes {

        glBindVertexArray(mesh.VAO)
        glDrawElementsInstanced(GL_TRIANGLES, GLsizei(mesh.indices.count), GL_UNSIGNED_INT, nil, GLsizei(modelMatrices.count))
        glBindVertexArray(0)
    }

    if model.disableCulling {
        glEnable(GL_CULL_FACE)
    }
}*/

void ModelInstanceRenderer::prepareModel(Model& model) {
    this->model = model;

    std::cout << "modelMatrices:" << modelMatrices.size() << std::endl;
    for( auto& mesh : model.meshes) {
        //print("v:\(mesh.vertices.count) i:\(mesh.indices.count)")
        // Vertex Buffer Object
        GLuint buffer;
        glBindVertexArray(mesh.VAO);
        glGenBuffers(1, &buffer);
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * modelMatrices.size(), &(modelMatrices.data()[0]), GL_STATIC_DRAW);
        // Vertex Attributes
        GLsizei mat4Size = sizeof(glm::mat4);

        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, mat4Size, (void*)0);
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, mat4Size, (void*)(sizeof(glm::vec4)));
        glEnableVertexAttribArray(5);
        glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, mat4Size, (void*)(2 * sizeof(glm::vec4)));
        glEnableVertexAttribArray(6);
        glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, mat4Size, (void*)(3 * sizeof(glm::vec4)));

        glVertexAttribDivisor(3, 1);
        glVertexAttribDivisor(4, 1);
        glVertexAttribDivisor(5, 1);
        glVertexAttribDivisor(6, 1);

        glBindVertexArray(0);
    }
}

void ModelInstanceRenderer::addInstance(glm::mat4& modelMatrix) {
    modelMatrices.push_back(modelMatrix);
}