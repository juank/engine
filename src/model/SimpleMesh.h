#pragma once

#include "../_lib/glad/glad.h"

class SimpleMesh {
public:
    GLuint vao;
    GLsizei count;

    SimpleMesh();
    SimpleMesh(GLuint vao, GLsizei count);
};