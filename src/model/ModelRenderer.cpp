#include "ModelRenderer.h"
#include <assimp/postprocess.h>
#include <iostream>
#include <sstream>
#include <GLFW/glfw3.h>
#include "../_lib/glm/gtc/matrix_transform.hpp"
#include "../util/Math.h"

ModelRenderer::ModelRenderer(): AbstractRenderer("../resources/shaders/mesh_skinned.vs", "../resources/shaders/mesh.fs"),
    entities(),
    entitiesToBlend() {
    //shaderBorder = Shader(vertexFile: "border.vs", fragmentFile: "border.fs")
}

void ModelRenderer::loadAnimation(std::string const &path) {
    // read file via ASSIMP
    animation = animationImporter.ReadFile(path, aiProcess_Triangulate);
    // check for errors
    if(!animation || !animation->mRootNode) // if is Not Zero
    {
        std::cout << "ERROR::ASSIMP::ANIMATION:: " << animationImporter.GetErrorString() << std::endl;
        return;
    }

    globalInverseTransform = mat4_cast(animation->mRootNode->mTransformation);
    globalInverseTransform = glm::inverse(globalInverseTransform);

    std::cout << animation->mAnimations[0]->mName.C_Str() << std::endl;
    std::cout << "loaded Anim" << std::endl;
}

void ModelRenderer::render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera) {

    // Activate shader
    shader.use();

    bindCommon(viewMatrix, projectionMatrix, camera);

    // shader.bindTexture("depthTextureSampler", tex: Sun.texDepth)
    //shader.bindTextureCubeMap("skybox", tex: skybox.texCubemap)

    shader.bindUniform("material.shininess", 32.0f);

    for( auto entity : entities ) {
            render(entity, viewMatrix, projectionMatrix);
    }

    /*entitiesToBlend.sortInPlace({ length(camera.position - $0.position) > length(camera.position - $1.position) })
    for entity in entitiesToBlend {
            render(entity, viewMatrix: &viewMatrix, projectionMatrix: &projectionMatrix)
    }*/
}

void ModelRenderer::render(ModelEntity* entity, glm::mat4& viewMatrix, glm::mat4& projectionMatrix) {
    glm::mat4 modelMatrix;
    modelMatrix = glm::translate(modelMatrix, entity->position);
    if( entity->pitch != 0.0 ) { modelMatrix = glm::rotate(modelMatrix, entity->pitch, PITCH_VECTOR); }
    if( entity->yaw != 0.0 ) { modelMatrix = glm::rotate(modelMatrix, entity->yaw, YAW_VECTOR); }
    if( entity->roll != 0.0 ) { modelMatrix = glm::rotate(modelMatrix, entity->roll, ROLL_VECTOR); }
    modelMatrix = glm::scale(modelMatrix, glm::vec3(entity->scale, entity->scale, entity->scale));
    shader.bindUniform("modelMatrix", modelMatrix);

    /*var selected = entity.selected

    if selected {
                // enable write on stencil
                glStencilFunc(GL_ALWAYS, 1, 0xFF)
                glStencilMask(0xFF)
        }*/

    float runningTime = GLfloat(glfwGetTime());

    if(entity->model.loadBones) {
        std::vector<glm::mat4> transforms;

        boneTransform(entity->model, runningTime, transforms);


        for (uint i = 0; i < transforms.size(); i++) {
            glUniformMatrix4fv(
                    glGetUniformLocation(shader.ID, (std::string("gBones[") + std::to_string(i) + std::string("]")).c_str()), 1,
                    GL_TRUE, &transforms[i][0][0]);
        }
    }

    for(unsigned int i = 0; i < entity->model.meshes.size(); i++) {
        render(entity->model.meshes[i]);
    }

    /*if selected {
                glStencilFunc(GL_NOTEQUAL, 1, 0xFF)
                glStencilMask(0x00)
                glDisable(GL_DEPTH_TEST)

                shaderBorder.use()
                /*modelMatrix = mat4()
                modelMatrix = SGLMath.translate(modelMatrix, entity.position)
                if entity.pitch != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.pitch, PITCH_VECTOR) }
                if entity.yaw != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.yaw, YAW_VECTOR) }
                if entity.roll != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.roll, ROLL_VECTOR) }
                modelMatrix = SGLMath.translate(modelMatrix, vec3(0, entity.model.top*entity.scale/2, 0))
                modelMatrix = SGLMath.scale(modelMatrix, vec3(1.1, 1.1, 1.1))
                modelMatrix = SGLMath.translate(modelMatrix, vec3(0, -entity.model.top*entity.scale/2, 0))
                modelMatrix = SGLMath.scale(modelMatrix, vec3(entity.scale, entity.scale, entity.scale))* /


                shaderBorder.bindUniform("viewMatrix", matrix: &viewMatrix)
                shaderBorder.bindUniform("projectionMatrix", matrix: &projectionMatrix)
                shaderBorder.bindUniform("modelMatrix", matrix: &modelMatrix)

                entity.model.draw(shaderBorder)

                //glStencilMask(0xFF)
                glEnable(GL_DEPTH_TEST)
                shader.use()
        }*/
}

void ModelRenderer::render(Mesh& mesh) {

    //std::cout << "shaderss.use:" << shader.ID << std::endl;
    bindTextures(mesh);

    // draw mesh
    glBindVertexArray(mesh.VAO);
    glDrawElements(GL_TRIANGLES, mesh.indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // always good practice to set everything back to defaults once configured.
    glActiveTexture(GL_TEXTURE0);
}

void ModelRenderer::bindTextures(Mesh& mesh) {
    // bind appropriate textures
    unsigned int diffuseNr  = 1;
    unsigned int specularNr = 1;
    unsigned int normalNr   = 1;
    unsigned int heightNr   = 1;
    //std::cout << "ok0" <<  std::endl;
    for(unsigned int i = 0; i < mesh.textures.size(); i++)
    {
        //std::cout << "ok1" << std::endl;
        GLenum err = glGetError();
        //std::cout << "ok2" << std::endl;
        //std::cout << "error:" << err << std::endl;
        glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
        //std::cout << "ok3" << std::endl;
        // retrieve texture number (the N in diffuse_textureN)
        std::stringstream ss;
        std::string number;
        std::string name = mesh.textures[i].type;
        if(name == "texture_diffuse")
            ss << diffuseNr++; // transfer unsigned int to stream
        else if(name == "texture_specular")
            ss << specularNr++; // transfer unsigned int to stream
        else if(name == "texture_normal")
            ss << normalNr++; // transfer unsigned int to stream
        else if(name == "texture_height")
            ss << heightNr++; // transfer unsigned int to stream
        number = ss.str();
        // now set the sampler to the correct texture unit
        glUniform1i(glGetUniformLocation(shader.ID, ("material."+name+number).c_str()), i);
        // and finally bind the texture
        glBindTexture(GL_TEXTURE_2D, mesh.textures[i].id);
    }
}

/*
public func depthRender(depthShader: Shader) {

    for entity in entities {
            depthRender(entity, depthShader: depthShader)
    }

    for entity in entitiesToBlend {
            depthRender(entity, depthShader: depthShader)
    }
}

private func depthRender(entity: ModelEntity, depthShader: Shader) {
    var modelMatrix = mat4()
    modelMatrix = SGLMath.translate(modelMatrix, entity.position)
    if entity.pitch != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.pitch, PITCH_VECTOR) }
    if entity.yaw != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.yaw, YAW_VECTOR) }
    if entity.roll != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.roll, ROLL_VECTOR) }
    modelMatrix = SGLMath.scale(modelMatrix, vec3(entity.scale, entity.scale, entity.scale))
    depthShader.bindUniform("modelMatrix", matrix: &modelMatrix)

    entity.model.draw(depthShader)
}*/

void ModelRenderer::addEntity(ModelEntity* entity) {
    if( entity->model.toBlend ) {
        entitiesToBlend.push_back(entity);
    } else {
        entities.push_back(entity);
    }
}

void ModelRenderer::boneTransform(Model &model, float timeInSeconds, std::vector<glm::mat4>& transforms) {
    glm::mat4 identity;

    float ticksPerSecond = (GLfloat)(animation->mAnimations[0]->mTicksPerSecond != 0 ? animation->mAnimations[0]->mTicksPerSecond : 25.0f);

    float timeInTicks = timeInSeconds * ticksPerSecond;
    float animationTime = fmod(timeInTicks, (float)animation->mAnimations[0]->mDuration);

    readNodeHeirarchy(model, animationTime, animation->mRootNode, identity);

    transforms.resize(model.bones.size());

    for (uint i = 0 ; i < model.bones.size() ; i++) {
        transforms[i] = model.boneInfos[i].finalTransformation;
    }
}

void ModelRenderer::readNodeHeirarchy(Model &model, float animationTime, const aiNode* pNode, const glm::mat4& parentTransform) {
    std::string NodeName(pNode->mName.data);

    const aiAnimation* pAnimation = animation->mAnimations[0];

    glm::mat4 nodeTransformation = mat4_cast(pNode->mTransformation);

    const aiNodeAnim* pNodeAnim = findNodeAnim(pAnimation, NodeName);

    if (pNodeAnim) {
        // Interpolate scaling and generate scaling transformation matrix
        aiVector3D aiScaling;
        calcInterpolatedScaling(aiScaling, animationTime, pNodeAnim);

        // Interpolate rotation and generate rotation transformation matrix
        aiQuaternion aiRotationQ;
        calcInterpolatedRotation(aiRotationQ, animationTime, pNodeAnim);

        // Interpolate translation and generate translation transformation matrix
        aiVector3D aiTranslation;
        calcInterpolatedPosition(aiTranslation, animationTime, pNodeAnim);

        aiMatrix4x4 transformation;
        composematrix(&transformation, &aiTranslation, &aiRotationQ, &aiScaling);

        nodeTransformation = glm::make_mat4(&transformation.a1);
    }

    glm::mat4 globalTransformation = mult(parentTransform, nodeTransformation);

    //NodeName = NodeName.substr(NodeName.find("_") + 1);

    if (model.bones.find(NodeName) != model.bones.end()) {
        uint BoneIndex = model.bones[NodeName];
        model.boneInfos[BoneIndex].finalTransformation = mult( mult(globalInverseTransform, globalTransformation), model.boneInfos[BoneIndex].boneOffset );

    } else {
        if(notFoundBones.find(NodeName) == notFoundBones.end() ) {
            std::cout << "TBONE:" << NodeName << std::endl;
            notFoundBones.insert(NodeName);
        }
    }

    for (uint i = 0 ; i < pNode->mNumChildren ; i++) {
        readNodeHeirarchy(model, animationTime, pNode->mChildren[i], globalTransformation);
    }
}

void ModelRenderer::calcInterpolatedPosition(aiVector3D& out, float animationTime, const aiNodeAnim* pNodeAnim) {
    if (pNodeAnim->mNumPositionKeys == 1) {
        out = pNodeAnim->mPositionKeys[0].mValue;
        return;
    }

    uint PositionIndex = findPosition(animationTime, pNodeAnim);
    uint NextPositionIndex = (PositionIndex + 1);
    assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
    float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
    float Factor = (animationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
    assert(Factor >= 0.0f && Factor <= 1.0f);
    const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
    const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
    aiVector3D Delta = End - Start;
    out = Start + Factor * Delta;
}

void ModelRenderer::calcInterpolatedRotation(aiQuaternion& out, float animationTime, const aiNodeAnim* pNodeAnim) {
    // we need at least two values to interpolate...
    if (pNodeAnim->mNumRotationKeys == 1) {
        out = pNodeAnim->mRotationKeys[0].mValue;
        return;
    }

    uint RotationIndex = findRotation(animationTime, pNodeAnim);
    uint NextRotationIndex = (RotationIndex + 1);
    assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
    float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
    float Factor = (animationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
    assert(Factor >= 0.0f && Factor <= 1.0f);
    const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
    const aiQuaternion& EndRotationQ   = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
    aiQuaternion::Interpolate(out, StartRotationQ, EndRotationQ, Factor);
    out = out.Normalize();
}

void ModelRenderer::calcInterpolatedScaling(aiVector3D& out, float animationTime, const aiNodeAnim* pNodeAnim) {
    if (pNodeAnim->mNumScalingKeys == 1) {
        out = pNodeAnim->mScalingKeys[0].mValue;
        return;
    }

    uint ScalingIndex = findScaling(animationTime, pNodeAnim);
    uint NextScalingIndex = (ScalingIndex + 1);
    assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
    float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
    float Factor = (animationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
    assert(Factor >= 0.0f && Factor <= 1.0f);
    const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
    const aiVector3D& End   = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
    aiVector3D Delta = End - Start;
    out = Start + Factor * Delta;
}

const aiNodeAnim* ModelRenderer::findNodeAnim(const aiAnimation* pAnimation, const std::string NodeName) {
    for (uint i = 0 ; i < pAnimation->mNumChannels ; i++) {
        const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];

        if (std::string(pNodeAnim->mNodeName.data) == NodeName) {
            return pNodeAnim;
        }
    }

    return NULL;
}

uint ModelRenderer::findPosition(float AnimationTime, const aiNodeAnim* pNodeAnim) {
    for (uint i = 0 ; i < pNodeAnim->mNumPositionKeys - 1 ; i++) {
        if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
            return i;
        }
    }

    assert(0);

    return 0;
}

uint ModelRenderer::findRotation(float AnimationTime, const aiNodeAnim* pNodeAnim) {
    assert(pNodeAnim->mNumRotationKeys > 0);

    for (uint i = 0 ; i < pNodeAnim->mNumRotationKeys - 1 ; i++) {
        if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
            return i;
        }
    }

    assert(0);

    return 0;
}

uint ModelRenderer::findScaling(float AnimationTime, const aiNodeAnim* pNodeAnim) {
    assert(pNodeAnim->mNumScalingKeys > 0);

    for (uint i = 0 ; i < pNodeAnim->mNumScalingKeys - 1 ; i++) {
        if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
            return i;
        }
    }

    assert(0);

    return 0;
}