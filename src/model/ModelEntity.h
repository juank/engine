#pragma once

#include "../_lib/glad/glad.h"
#include "../_lib/glm/vec3.hpp"
#include "Model.h"

class ModelEntity {
public:
    Model model;
    glm::vec3 position;
    GLfloat scale;
    GLfloat pitch; // X
    GLfloat yaw; // Y
    GLfloat roll; // Z

    bool selected = false;

    ModelEntity(Model model, glm::vec3 position, GLfloat scale = 1, GLfloat pitch = 0, GLfloat yaw = 0, GLfloat roll = 0);
};