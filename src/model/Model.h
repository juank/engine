#pragma once

#include "Mesh.h"
#include <vector>
#include <map>

class Model {
public:
    struct BoneInfo {
        glm::mat4 boneOffset;
        glm::mat4 finalTransformation;
    };

    // Model Data
    std::vector<Texture> textures_loaded;	// stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
    std::vector<Mesh> meshes;
    std::string directory;
    bool gammaCorrection;

    bool toBlend = false;

    bool hasTransparency = false;

    std::map<std::string,uint> bones; // maps a bone name to its index
    std::vector<BoneInfo> boneInfos;

    std::string filePath;
    bool loadBones = false;

    Model();
    Model(std::string const &path, bool loadBones = false, bool gamma = false);
};