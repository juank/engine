#pragma once

#include "../core/AbstractRenderer.h"
#include <vector>
#include "ModelEntity.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <unordered_set>

class ModelRenderer: public AbstractRenderer {
private:
    std::vector<ModelEntity*> entities;
    std::vector<ModelEntity*> entitiesToBlend;

    glm::vec3 PITCH_VECTOR = glm::vec3(1.0, 0.0, 0.0);
    glm::vec3 YAW_VECTOR = glm::vec3(0.0, 1.0, 0.0);
    glm::vec3 ROLL_VECTOR = glm::vec3(0.0, 0.0, 1.0);

    Assimp::Importer animationImporter;
    const aiScene* animation;
    glm::mat4 globalInverseTransform;
    std::unordered_set<std::string> notFoundBones;
    //let shaderBorder: Shader
public:
    ModelRenderer();

    void loadAnimation(std::string const &path);
    void render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera);
    void render(ModelEntity* entity, glm::mat4& viewMatrix, glm::mat4& projectionMatrix);
    void render(Mesh& mesh);
    void bindTextures(Mesh& mesh);
    //func depthRender(depthShader: Shader);
    //func depthRender(entity: ModelEntity, depthShader: Shader);
    void addEntity(ModelEntity* entity);

private:
    void boneTransform(Model &model, float timeInSeconds, std::vector<glm::mat4>& transforms);
    void readNodeHeirarchy(Model &model, float animationTime, const aiNode* pNode, const glm::mat4& parentTransform);
    void calcInterpolatedPosition(aiVector3D& out, float animationTime, const aiNodeAnim* pNodeAnim);
    void calcInterpolatedRotation(aiQuaternion& out, float animationTime, const aiNodeAnim* pNodeAnim);
    void calcInterpolatedScaling(aiVector3D& out, float animationTime, const aiNodeAnim* pNodeAnim);
    const aiNodeAnim* findNodeAnim(const aiAnimation* pAnimation, const std::string NodeName);
    uint findPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
    uint findRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
    uint findScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
};
