#pragma once

#include "Model.h"
#include "../core/OpenGLLoader.h"
#include <assimp/scene.h>

class ModelLoader {
public:
    ModelLoader();

    // loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
    Model loadModel(std::string const &path, bool loadBones=false);
    void setTexture(Model& model, const std::string& fileName, OpenGLLoader& loader);

private:
    // processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
    void processNode(Model& model, aiNode *node, const aiScene *scene);
    Mesh processMesh(Model& model, const aiMesh* mesh, const aiScene* scene);

    // checks all material textures of a given type and loads the textures if they're not loaded yet.
    // the required info is returned as a Texture struct.
    std::vector<Texture> loadMaterialTextures(Model& model, aiMaterial *mat, aiTextureType type, std::string typeName);
    void loadBones(Model& model, const aiMesh* pMesh, std::vector<VertexBoneData>& meshBones);
    unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma=false);
};
