#pragma once

#include "../_lib/glm/glm.hpp"
#include <vector>
#include <string>
#include <assimp/types.h>

#ifndef uint
#define uint unsigned int
#endif

struct Vertex {
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};

struct Texture {
    unsigned int id;
    std::string type;
    aiString path;
};

#define NUM_BONES_PER_VEREX 4
struct VertexBoneData
{
    uint ids[NUM_BONES_PER_VEREX];
    float weights[NUM_BONES_PER_VEREX] = {0.0f, 0.0f, 0.0f, 0.0f};

    void addBoneData(uint boneID, float weight) {
        //cout << "addBone: " << boneID << " " << weight << endl;
        for (uint i = 0 ; i < NUM_BONES_PER_VEREX ; i++) {
            if (weights[i] == 0.0) {
                ids[i]     = boneID;
                weights[i] = weight;

                //std::cout << "addBone: " << boneID << " " << weight << std::endl;
                return;
            }
        }

        // should never get here - more bones than we have space for
        assert(0);
    }
};

class Mesh {
public:
    /*  Mesh Data  */
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;
    unsigned int VAO;

    std::vector<VertexBoneData> bones;

    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures, std::vector<VertexBoneData> bones);

private:
    // Render data
    unsigned int VBO, EBO, BONE_VBO;

    void setupMesh();
};


