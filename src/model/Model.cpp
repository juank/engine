#include "Model.h"
#include "../_lib/stb/stb_image.h"
#include "../core/Shader.h"

#define GLM_ENABLE_EXPERIMENTAL

Model::Model() {
}

Model::Model(std::string const &path, bool loadBones, bool gamma) :
    filePath{path},
    directory{path.substr(0, path.find_last_of('/'))},
    gammaCorrection{gamma},
    loadBones{loadBones} {
}




