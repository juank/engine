#define _CRT_RAND_S
#include <stdlib.h>


#include "_lib/stb/stb_image.h"

#include "_lib/glm/glm.hpp"
#include "_lib/glm/gtc/matrix_transform.hpp"

#include "Display.h"
#include "input/Input.h"
#include "camera/Camera.h"
#include <iostream>
#include <thread>

#include "CubeModel.h"
#include "core/OpenGLLoader.h"
#include "model/Model.h"
#include "core/Clock.h"
#include "core/Renderer.h"
#include "model/ModelLoader.h"
#include "Player.h"

#include "_lib/imgui/imgui.h"
#include "_lib/imgui/imgui_impl_glfw.h"
#include "_lib/imgui/imgui_impl_opengl3.h"
#include "system/TerrainSystem.h"



//#include <assimp/DefaultLogger.hpp>

Terrain loadTerrains(Renderer& renderer);
void loadModelEntities(ModelLoader& modelLoader, OpenGLLoader& loader, Renderer& renderer);
Player* loadPlayer(ModelLoader& modelLoader, OpenGLLoader& loader, Renderer& renderer, Camera* camera, Terrain* terrain);
void loadTrees(ModelLoader& modelLoader, OpenGLLoader& loader, Renderer& renderer, Terrain& terrain);

int TERRAIN_SEED = 100000;
float TERRAIN_SCALE = 1;
int TERRAIN_OCTAVES = 1;
float TERRAIN_PERSISTANCE = 1;
float TERRAIN_LACUNARITY = 1;
int LEVEL_OF_DETAIL = 0;

void test() {
    std::cout << "runnnnnnn" << std::endl;
}

int main()
{
    // camera
    Camera camera(glm::vec3(0.0f, 5.0f, 100.0f));

    Display display(1600, 1000);


    // Setup Dear ImGui binding
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

    ImGui_ImplGlfw_InitForOpenGL(display.window, true);
    ImGui_ImplOpenGL3_Init();

    // Setup style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    OpenGLLoader openGLLoader;
    ModelLoader modelLoader;

    Renderer renderer(display, openGLLoader);

    Terrain terrain = loadTerrains(renderer);
    //loadModelEntities(modelLoader, openGLLoader, renderer);
    Player* player = loadPlayer(modelLoader, openGLLoader, renderer, &camera, &terrain);
    loadTrees(modelLoader, openGLLoader, renderer, terrain);

    GLuint texCubemap = openGLLoader.loadCubemapTexture("../resources/textures/skybox/");
    Skybox skybox;
    skybox.texCubemap = texCubemap;
    renderer.setSkybox(skybox);


    Input input(display, player);

    bool enablePolygonMode = false;

    std::cout << "thread main " << std::this_thread::get_id() << std::endl;
    TerrainSystem terrainSystem(terrain, *player);
    std::thread terrainSystemThread(&TerrainSystem::run, &terrainSystem);
    //std::thread t1(test);
    std::cout << "started terrain" << std::endl;
    std::cout << "thread main2 " << std::this_thread::get_id() << std::endl;

    while (!display.shouldClose())
    {
        // per-frame time logic
        // --------------------
        Clock::frameStart();

        // input
        // -----
        input.processInput();

        // Start the ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // 1. Show a simple window.
        // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets automatically appears in a window called "Debug".
        {
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

            ImGui::Text("Terrain");
            ImGui::SliderInt("seed", &TERRAIN_SEED, 0, 100000);
            ImGui::SliderFloat("scale", &TERRAIN_SCALE, 0.01, 10.0);
            ImGui::SliderInt("octaves", &TERRAIN_OCTAVES, 0, 10);
            ImGui::SliderFloat("persistance", &TERRAIN_PERSISTANCE, 0.0, 10.0);
            ImGui::SliderFloat("lacunarity", &TERRAIN_LACUNARITY, 0.0, 10.0);
            ImGui::SliderInt("lod", &LEVEL_OF_DETAIL, 0, 6);


            if (ImGui::Button("Reload")){
                std::cout << "reload" << std::endl;
                renderer.clearTerrains();
                terrain = loadTerrains(renderer);
            }

            if (ImGui::Button("PolygonMode")){
                enablePolygonMode = !enablePolygonMode;
                if(enablePolygonMode) {
                    renderer.enablePolygonMode();
                } else {
                    renderer.disablePolygonMode();
                }
            }


        }

        // Rendering
        ImGui::Render();

        renderer.load();
        renderer.render(camera);

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());


        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        display.swapBuffers();
        input.pollEvents();
    }

    terrainSystem.stop();
    terrainSystemThread.join();

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    //glDeleteVertexArrays(1, &VAO);
    //glDeleteBuffers(1, &VBO);

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    return 0;
}

Terrain loadTerrains(Renderer& renderer) {
    Terrain terrain(renderer);
    terrain.load(0, 0);
    terrain.load(1, 0);
    renderer.load();
    return terrain;
}

/*class myStream :
        public Assimp::LogStream
{
public:
    // Constructor
    myStream()
    {
        // empty
    }

    // Destructor
    ~myStream()
    {
        // empty
    }
    // Write womethink using your own functionality
    void write(const char* message)
    {
        std::cout << message << std::endl;
    }
};*/

Player* loadPlayer(ModelLoader& modelLoader, OpenGLLoader& loader, Renderer& renderer, Camera* camera, Terrain* terrain) {
    Model model = modelLoader.loadModel("../resources/assets/kachujin/kachujin_g_rosales.dae", true);
    renderer.loadAnimation("../resources/assets/kachujin/animation/walk.dae");

    ModelEntity* entity = new ModelEntity(model, glm::vec3(TerrainChunk::MAP_CHUNK_SIZE/2, 0.0, TerrainChunk::MAP_CHUNK_SIZE/2));
    //entity->yaw = -90;
    entity->scale = 0.03;
    //entity.selected = true
    renderer.addModelEntity(entity);

    Player* player = new Player(entity, camera, terrain);

    return player;
}

void loadModelEntities(ModelLoader& modelLoader, OpenGLLoader& loader, Renderer& renderer) {
    //Model model("../resources/assets/nanosuit/nanosuit.obj");

    Model model = modelLoader.loadModel("../resources/assets/kachujin/kachujin_g_rosales.dae", true);
    renderer.loadAnimation("../resources/assets/kachujin/animation/walk.dae");

    // Create a logger instance
    /*Assimp::DefaultLogger::create("", Assimp::Logger::VERBOSE);
    const unsigned int severity = Assimp::Logger::Debugging|Assimp::Logger::Info|Assimp::Logger::Err|Assimp::Logger::Warn;
    Assimp::DefaultLogger::get()->attachStream( new myStream(), severity );
    // Now I am ready for logging my stuff
    Assimp::DefaultLogger::get()->info("this is my info-call");*/


    // Kill it after the work is done
    //Assimp::DefaultLogger::kill();

    //ModelEntity entity(model, glm::vec3(1.0, 0.0, 20.0));
    ModelEntity* entity = new ModelEntity(model, glm::vec3(0.0, 0.0, 20.0));
    //entity.pitch = -90;
    entity->scale = 0.03;
    //entity.selected = true
    renderer.addModelEntity(entity);

   /* model = Model2(path: "Resources/assets/ellie/Ellie.obj")
    model.hasTransparency = true
    model.disableCulling = true
    entity = ModelEntity(model: model, position: vec3(360.0, terrain.getHeight(360, 400), 400.0), scale: 4, pitch: 0, yaw: 0, roll: 0)
    renderer.addModelEntity(entity)*/

    //loadGrass(loader, renderer: renderer)

}


float drand48() {
    unsigned int number;
    rand_s(&number);
    return (float) number / (float) UINT_MAX;
}

glm::vec3 randomPosition(Terrain& terrain) {
    GLfloat x = GLfloat(drand48()*TerrainChunk::MAP_CHUNK_SIZE);
    GLfloat z = GLfloat(drand48()*TerrainChunk::MAP_CHUNK_SIZE);
    return glm::vec3(x, terrain.getHeight(x, z), z);
}

glm::vec3 PITCH_VECTOR = glm::vec3(1.0, 0.0, 0.0);
glm::vec3 YAW_VECTOR = glm::vec3(0.0, 1.0, 0.0);
glm::vec3 ROLL_VECTOR = glm::vec3(0.0, 0.0, 1.0);

void loadTrees(ModelLoader& modelLoader, OpenGLLoader& loader, Renderer& renderer, Terrain& terrain) {
    Model model = modelLoader.loadModel("../resources/assets/trees/pine.obj");
    modelLoader.setTexture(model, "pine.png", loader);

    //Model model("../resources/assets/trees/trees2/Group3.3ds", "Blossomi");


    model.hasTransparency = true;
    //model.disableCulling = true

    for( int i=0; i<10; i++) {

        glm::mat4 modelMatrix;
        modelMatrix = glm::translate(modelMatrix, randomPosition(terrain));
       // modelMatrix = glm::rotate(modelMatrix, glm::radians(-90.0f), PITCH_VECTOR);
        /*if entity.pitch != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.pitch, PITCH_VECTOR) }
        if entity.yaw != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.yaw, YAW_VECTOR) }
        if entity.roll != 0.0 { modelMatrix = SGLMath.rotate(modelMatrix, entity.roll, ROLL_VECTOR) }*/
        //float scale = 30.0;
        //modelMatrix = glm::scale(modelMatrix, glm::vec3(scale, scale, scale));

        renderer.addModelInstace(modelMatrix);
    }

    renderer.prepareModelInstace(model);
}




