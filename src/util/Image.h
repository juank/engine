#pragma once

class Image {
public:
    unsigned char *data = nullptr;
    int width;
    int height;
    int nrChannels;

    Image(unsigned char *data, int width, int height, int nrChannels) :
        width(width),
        height(height),
        nrChannels(nrChannels) {
        this->data = data;
    }

    unsigned char getRed(int x, int y) {
        return data[ ( nrChannels * ( (y * width) + x ) ) + 0];
    }
};
