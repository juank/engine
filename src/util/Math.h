#pragma once

#include "../_lib/glad/glad.h"
#include "../_lib/glm/vec3.hpp"
#include "../_lib/glm/vec2.hpp"
#include "../_lib/glm/gtc/type_ptr.hpp"
#include <assimp/types.h>

class Math {
public:
    static GLfloat barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) {
        GLfloat det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
        GLfloat l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
        GLfloat l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
        GLfloat l3 = 1.0 - l1 - l2;
        return l1 * p1.y + l2 * p2.y + l3 * p3.y;
    }
};

static inline glm::vec3 vec3_cast(const aiVector3D &v) { return glm::vec3(v.x, v.y, v.z); }
static inline glm::vec2 vec2_cast(const aiVector3D &v) { return glm::vec2(v.x, v.y); } // it's aiVector3D because assimp's texture coordinates use that
static inline glm::quat quat_cast(const aiQuaternion &q) { return glm::quat(q.w, q.x, q.y, q.z); }
static inline glm::mat4 mat4_cast(const aiMatrix4x4 &m) { return glm::transpose(glm::make_mat4(&m.a1)); }
static inline glm::mat3 mat3_cast(const aiMatrix3x3 &m) { return glm::transpose(glm::make_mat3(&m.a1)); }

static inline glm::mat4 mult(const glm::mat4& left, const glm::mat4& right) {
    glm::mat4 ret;

    for (unsigned int i = 0 ; i < 4 ; i++) {
        for (unsigned int j = 0 ; j < 4 ; j++) {
            ret[i][j] = left[i][0] * right[0][j] +
                left[i][1] * right[1][j] +
                left[i][2] * right[2][j] +
                left[i][3] * right[3][j];
        }
    }
    return ret;
}

static inline void composematrix(aiMatrix4x4 *m, aiVector3D *t, aiQuaternion *q, aiVector3D *s) {
    // quat to rotation matrix
    m->a1 = 1 - 2 * (q->y * q->y + q->z * q->z);
    m->a2 = 2 * (q->x * q->y - q->z * q->w);
    m->a3 = 2 * (q->x * q->z + q->y * q->w);
    m->b1 = 2 * (q->x * q->y + q->z * q->w);
    m->b2 = 1 - 2 * (q->x * q->x + q->z * q->z);
    m->b3 = 2 * (q->y * q->z - q->x * q->w);
    m->c1 = 2 * (q->x * q->z - q->y * q->w);
    m->c2 = 2 * (q->y * q->z + q->x * q->w);
    m->c3 = 1 - 2 * (q->x * q->x + q->y * q->y);

    // scale matrix
    m->a1 *= s->x; m->a2 *= s->x; m->a3 *= s->x;
    m->b1 *= s->y; m->b2 *= s->y; m->b3 *= s->y;
    m->c1 *= s->z; m->c2 *= s->z; m->c3 *= s->z;

    // set translation
    m->a4 = t->x; m->b4 = t->y; m->c4 = t->z;

    m->d1 = 0; m->d2 = 0; m->d3 = 0; m->d4 = 1;
}

static inline float inverseLerp(GLfloat& min, GLfloat& max, GLfloat& value) {
    return (value-min)/(max-min);
}