#pragma once

#include "../core/AbstractRenderer.h"
#include "TerrainChunk.h"
#include "../core/OpenGLLoader.h"

class TerrainRenderer : public AbstractRenderer {
    OpenGLLoader* openGlLoader;
public:
    std::vector<TerrainChunk> terrains;
    std::vector<TerrainChunk> terrainsToLoad;
    std::vector<glm::vec2> terrainsToUnload;

    TerrainRenderer(OpenGLLoader& openGLLoader);

    void render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera);
    void addTerrain(TerrainChunk& terrain);
    void removeTerrain(glm::vec2 position);
    void clear();
    void load();
};
