#include "NoiseMapGenerator.h"
#include <random>
#include "../util/FastNoise.h"
#include <limits>
#include <iostream>

std::vector< std::vector<GLfloat > > NoiseMapGenerator::generateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, glm::vec2 offset) {
    auto noiseMap = std::vector< std::vector<GLfloat> >(mapWidth+1, std::vector<GLfloat>(mapHeight+1, 0.0f));

    FastNoise noise;
    noise.SetSeed(seed);

    std::cout << "seed:" << seed << std::endl;
    std::mt19937 random(seed); // Fixed seed of 0
    std::uniform_real_distribution<> dist;

    float maxPossibleHeight = 0;
    float amplitude = 1;
    float frequency = 1;

    std::vector<glm::vec2> octaveOffsets;
    for (int i = 0; i < octaves; i++) {
        GLfloat offsetX = /*dist(random) +*/ offset.x;
        GLfloat offsetY = /*dist(random) + */offset.y;

        //GLfloat offsetX = 0;
        //GLfloat offsetY = 0;
        octaveOffsets.push_back(glm::vec2(offsetX, offsetY));

        maxPossibleHeight += amplitude;
        amplitude *= persistance;
    }

    if (scale <= 0) {
        scale = 0.0001f;
    }

    float maxNoiseHeight = std::numeric_limits<float>::min();
    float minNoiseHeight = std::numeric_limits<float>::max();

    float halfWidth = (float)mapWidth / 2.0;
    float halfHeight = (float)mapHeight / 2.0;


    for (int y = 0; y <= mapHeight; y++) {
        for (int x = 0; x <= mapWidth; x++) {

            amplitude = 1;
            frequency = 1;
            float noiseHeight = 0;

            for (int i = 0; i < octaves; i++) {
                float sampleX = (x-halfWidth+ octaveOffsets[i].x) / scale * frequency;
                float sampleY = (y-halfHeight+ octaveOffsets[i].y) / scale * frequency;

                float perlinValue = noise.GetPerlin(sampleX, sampleY);// * 2 - 1;
                noiseHeight += perlinValue * amplitude;

                amplitude *= persistance;
                frequency *= lacunarity;
            }

            if (noiseHeight > maxNoiseHeight) {
                maxNoiseHeight = noiseHeight;
            } else if (noiseHeight < minNoiseHeight) {
                minNoiseHeight = noiseHeight;
            }
            noiseMap[x][y] = noiseHeight;

        }
    }

    std::cout << "min:" << minNoiseHeight << " max:" << maxNoiseHeight << " maxPos:" << maxPossibleHeight << std::endl;

    for (int y = 0; y <= mapHeight; y++) {
        for (int x = 0; x <= mapWidth; x++) {
            //noiseMap [x][y] = inverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap [x][y]);
            //noiseMap [x][y] = 100.0 * noiseMap [x][y];

            float normalizedHeight = noiseMap [x][y] / maxPossibleHeight + 0.5;
            noiseMap [x][y] = 100.0 * normalizedHeight;
            //std::cout << "noise:" <<noiseMap[x][y] << std::endl;
        }
    }

    return noiseMap;
}