#include "TerrainRenderer.h"

TerrainRenderer::TerrainRenderer(OpenGLLoader& openGlLoader) :
    AbstractRenderer{"../resources/shaders/terrain.vs", "../resources/shaders/terrain.fs"},
    terrains{},
    openGlLoader{&openGlLoader} {
}

void TerrainRenderer::load() {
    for(auto& terrain : terrainsToLoad) {
        terrain.model = openGlLoader->loadVao(terrain.vertices, terrain.texCoords, terrain.normals, terrain.indices);
        //terrain.vertices.clear();
        //terrain.texCoords.clear();
        //terrain.normals.clear();
        //terrain.indices.clear();
        terrains.push_back(terrain);
    }
    terrainsToLoad.clear();

    //TODO this isn't optimal
    for(auto& position : terrainsToUnload) {
        for(auto iterator = terrains.begin(); iterator != terrains.end(); ) {
            if(iterator->chunkPosition.x == position.x && iterator->chunkPosition.y == position.y) {
                iterator = terrains.erase(iterator);
                //TODO delete from opengl
            } else {
                ++iterator;
            }
        }
    }
    terrainsToUnload.clear();
}

void TerrainRenderer::render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera) {

    // Activate shader
    shader.use();

    bindCommon(viewMatrix, projectionMatrix, camera);

    //shader.bindTexture("depthTextureSampler", Sun.texDepth);



    for(auto terrain : terrains) {
        // Draw container
        //std::cout << "bind vao:" << terrain.model.vao << std::endl;
        glBindVertexArray(terrain.model.vao);


        glm::mat4 modelMatrix;
        shader.bindUniform("modelMatrix", modelMatrix);
        glDrawElements(GL_TRIANGLES, terrain.model.count, GL_UNSIGNED_INT, nullptr);

        glBindVertexArray(0); // unbind vao
    }
}

void TerrainRenderer::addTerrain(TerrainChunk& terrain) {
    terrainsToLoad.push_back(terrain);
}

void TerrainRenderer::removeTerrain(glm::vec2 position) {
    terrainsToUnload.push_back(position);
}

void TerrainRenderer::clear() {
    for(auto terrain : terrains) {
        glDeleteVertexArrays(1, &terrain.model.vao);
        //glDeleteBuffers(1, &VBO);
    }
    terrains.clear();
}