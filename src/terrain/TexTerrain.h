#pragma once

#include <vector>
#include "../_lib/glad/glad.h"

class TexTerrain {
public:
    std::vector<GLuint> texs;
    GLuint blendTex;

    TexTerrain(std::vector<GLuint> texs, GLuint blendTex);

    bool operator==(const TexTerrain &rhs) const;
};