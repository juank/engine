#pragma once

#include "../core/Renderer.h"

class Terrain {
    Renderer* renderer;

    NoiseMapGenerator noiseMapGenerator;

    TerrainChunk *get(GLint x, GLint y);
    GLint getChunkIndex(const GLfloat& value);

    GLint LOAD_RADIUS;
    GLint UNLOAD_RADIUS;

public:
    std::unordered_map<GLint, std::unordered_map<GLint, TerrainChunk>> terrainChunks;

    Terrain(Renderer& renderer);

    GLfloat getHeight(GLfloat x, GLfloat y);
    void load(GLint xChunk, GLint yChunk);
    void update(ModelEntity& entity);

    void updateChunks(glm::vec3& position);
};
