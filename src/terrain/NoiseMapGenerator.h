#pragma once

#include "../_lib/glad/glad.h"
#include "../_lib/glm/vec2.hpp"
#include <vector>

class NoiseMapGenerator {
public:
    std::vector<std::vector<GLfloat>> generateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, glm::vec2 offset);
};
