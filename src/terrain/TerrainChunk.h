#pragma once

#include "NoiseMapGenerator.h"
#include "../model/SimpleMesh.h"

class TerrainChunk {
    std::vector< std::vector<GLfloat > > heights;
    NoiseMapGenerator noiseMapGenerator;



public:
    static constexpr GLfloat MAP_CHUNK_SIZE = 240;
    glm::vec2 chunkPosition;
    SimpleMesh model;

    //TODO check
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> texCoords;
    std::vector<GLuint> indices;

    TerrainChunk();
    TerrainChunk(glm::vec2 chunkPosition, NoiseMapGenerator& noiseMapGenerator);

    void generateTerrain();
    GLfloat getHeight(GLfloat worldX, GLfloat worldZ);
    glm::vec3 calculateNormal(int x, int y);
};