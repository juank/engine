#include "Terrain.h"
#include "TexTerrain.h"
#include "TerrainChunk.h"
#include <iostream>


extern int TERRAIN_SEED;
extern float TERRAIN_SCALE ;
extern int TERRAIN_OCTAVES;
extern float TERRAIN_PERSISTANCE;
extern float TERRAIN_LACUNARITY;
extern int LEVEL_OF_DETAIL;

Terrain::Terrain(Renderer& renderer) :
        renderer{&renderer},
        noiseMapGenerator{},
        terrainChunks{},
        LOAD_RADIUS{1},
        UNLOAD_RADIUS{2} {

}

GLfloat Terrain::getHeight(GLfloat x, GLfloat y) {
    GLint xChunk = getChunkIndex(x);
    GLint yChunk = getChunkIndex(y);

    //std::cout << "chunk: " << xChunk << ":" << yChunk << std::endl;
    TerrainChunk* terrainChunk = get(xChunk, yChunk);
    if (terrainChunk != nullptr) {
        return terrainChunk->getHeight(x, y);
    } else {
        return 0;
    }

    return 0;
}

TerrainChunk* Terrain::get(GLint x, GLint y) {
    TerrainChunk* terrainChunk = nullptr;
    if (terrainChunks.find(x) != terrainChunks.end()) {
        if (terrainChunks[x].find(y) != terrainChunks[x].end()) {
            terrainChunk = &terrainChunks[x][y];
        }
    }
    return terrainChunk;
}

void Terrain::load(GLint xChunk, GLint yChunk) {
    glm::vec2 chunkPosition(xChunk, yChunk);
    TerrainChunk terrainChunk(chunkPosition,noiseMapGenerator);
    terrainChunks[xChunk][yChunk] = terrainChunk;
    renderer->addTerrain(terrainChunk);
}

void Terrain::update(ModelEntity& entity) {
    entity.position.y = getHeight(entity.position.x, entity.position.z);
}

void Terrain::updateChunks(glm::vec3& position) {
    GLint xChunkCenter = getChunkIndex(position.x);
    GLint yChunkCenter = getChunkIndex(position.z);
    std::cout << "position:" << position.x << ":" << position.z << "  chunk: " << xChunkCenter << ":" << yChunkCenter << std::endl;

    for(auto xIterator = terrainChunks.begin(); xIterator != terrainChunks.end(); ++xIterator) {
        GLint xChunk = xIterator->first;
        for(auto yIterator = xIterator->second.begin(); yIterator != xIterator->second.end(); ) {
            GLint yChunk = yIterator->first;
            if(abs(xChunkCenter-xChunk) > UNLOAD_RADIUS || abs(yChunkCenter-yChunk) > UNLOAD_RADIUS) {
                std::cout << "remove chunk " << xChunk << " " << yChunk << std::endl;
                renderer->removeTerrain(yIterator->second.chunkPosition);
                xIterator->second.erase(yIterator++);
            } else {
                ++yIterator;
            }
        }
    }

    for (GLint xChunk = xChunkCenter - LOAD_RADIUS; xChunk <= xChunkCenter + LOAD_RADIUS; ++xChunk) {
        for (GLint yChunk = yChunkCenter - LOAD_RADIUS; yChunk <= yChunkCenter + LOAD_RADIUS; ++yChunk) {
            TerrainChunk *terrainChunk = get(xChunk, yChunk);
            if (terrainChunk == nullptr) {
                load(xChunk, yChunk);
            }
        }
    }
}

GLint Terrain::getChunkIndex(const GLfloat& value) {
    GLint chunkIndex = value / TerrainChunk::MAP_CHUNK_SIZE;
    if(value < 0) {
        --chunkIndex;
    }
    return chunkIndex;
}
