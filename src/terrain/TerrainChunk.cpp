#include "TerrainChunk.h"
#include "../util/Math.h"
#include <iostream>

extern int TERRAIN_SEED;
extern float TERRAIN_SCALE ;
extern int TERRAIN_OCTAVES;
extern float TERRAIN_PERSISTANCE;
extern float TERRAIN_LACUNARITY;
extern int LEVEL_OF_DETAIL;

TerrainChunk::TerrainChunk() {
}

TerrainChunk::TerrainChunk(glm::vec2 chunkPosition, NoiseMapGenerator& noiseMapGenerator) :
        chunkPosition{chunkPosition},
        model{},//TODO check
        noiseMapGenerator{noiseMapGenerator}{
    std::cout << "TerrainChunk create" << std::endl;
    generateTerrain();
}

void TerrainChunk::generateTerrain() {
    //heights = std::vector< std::vector<GLfloat> >(verticesPerLine, std::vector<GLfloat>(verticesPerLine, 0.0f));
    int mapWidth = MAP_CHUNK_SIZE;
    int mapHeight = MAP_CHUNK_SIZE;
    int seed = TERRAIN_SEED;
    float scale = TERRAIN_SCALE;
    int octaves = TERRAIN_OCTAVES;
    float persistance = TERRAIN_PERSISTANCE;
    float lacunarity = TERRAIN_LACUNARITY;
    glm::vec2 offset(chunkPosition.x*MAP_CHUNK_SIZE, chunkPosition.y*MAP_CHUNK_SIZE);
    heights = noiseMapGenerator.generateNoiseMap(mapWidth, mapHeight, seed, scale, octaves, persistance, lacunarity, offset);
    std::cout << "ok:" << std::endl;
    int lod = LEVEL_OF_DETAIL == 0 ? 1 : LEVEL_OF_DETAIL * 2;
    GLint verticesPerLine = MAP_CHUNK_SIZE / lod + 1;
    int verticesCount = verticesPerLine * verticesPerLine;
    vertices = std::vector<GLfloat>(verticesCount * 3, 0.0f);
    normals = std::vector<GLfloat>(verticesCount * 3, 0.0f);
    texCoords = std::vector<GLfloat>(verticesCount * 2, 0.0f);
    indices = std::vector<GLuint>(6 * (verticesPerLine-1) * (verticesPerLine-1), 0);
    int verticePointer = 0;
    for( int y=0; y<=MAP_CHUNK_SIZE; y+=lod ) {
        for( int x=0; x<=MAP_CHUNK_SIZE; x+=lod ) {
            auto height = heights[x][y];

            vertices[(verticePointer*3)] = GLfloat(x)+offset.x;
            vertices[(verticePointer*3)+1] = height;
            vertices[(verticePointer*3)+2] = GLfloat(y)+offset.y;
            auto normal = calculateNormal(x+offset.x, y+offset.y);
            normals[verticePointer*3] = normal.x;
            normals[(verticePointer*3)+1] = normal.y;
            normals[(verticePointer*3)+2] = normal.z;
            texCoords[verticePointer*2] = GLfloat(x)/GLfloat(MAP_CHUNK_SIZE);
            texCoords[(verticePointer*2)+1] = GLfloat(y)/GLfloat(MAP_CHUNK_SIZE);



            /*std::cout << "v:" << verticePointer << "-> " << vertices[(verticePointer*3)] << ", " << vertices[(verticePointer*3)+1]
                    << ", " << vertices[(verticePointer*3)+2] << std::endl;*/
            /*std::cout << "uv:" << verticePointer << "-> " << texCoords[(verticePointer*2)] << ", " << texCoords[(verticePointer*2)+1]
                       << std::endl;*/

            verticePointer++;
        }
    }
    int pointer = 0;
    for( int gz=0; gz<verticesPerLine-1; gz++) {
        for( int gx=0; gx<verticesPerLine-1; gx++) {
            int topLeft = (gz*verticesPerLine)+gx;
            int topRight = topLeft + 1;
            int bottomLeft = ((gz+1)*verticesPerLine)+gx;
            int bottomRight = bottomLeft + 1;
            indices[pointer++] = GLuint(topLeft);
            indices[pointer++] = GLuint(bottomLeft);
            indices[pointer++] = GLuint(topRight);
            indices[pointer++] = GLuint(topRight);
            indices[pointer++] = GLuint(bottomLeft);
            indices[pointer++] = GLuint(bottomRight);
        }
    }
}

GLfloat TerrainChunk::getHeight(GLfloat worldX, GLfloat worldZ) {
    GLfloat terrainX = worldX;
    GLfloat terrainZ = worldZ;
    GLfloat gridSquareSize = MAP_CHUNK_SIZE / GLfloat(heights.size() - 1);

    terrainX -= MAP_CHUNK_SIZE * chunkPosition.x;
    terrainZ -= MAP_CHUNK_SIZE * chunkPosition.y;


    int xTile = int(floor(double(terrainX / gridSquareSize)));
    int zTile = int(floor(double(terrainZ / gridSquareSize)));
    if( xTile >= heights.size() - 1 || zTile >= heights.size() - 1 || xTile < 0 || zTile < 0 ) {
        return 0;
    }

    GLfloat xCoord = fmod(terrainX, gridSquareSize) / gridSquareSize;
    GLfloat zCoord = fmod(terrainZ, gridSquareSize) / gridSquareSize;

    GLfloat answer;
    if( xCoord <= (1-zCoord) ) {
        answer = Math::barryCentric(glm::vec3(0, heights[xTile][zTile], 0),
                                    glm::vec3(1, heights[xTile + 1][zTile], 0),
                                    glm::vec3(0, heights[xTile][zTile + 1], 1),
                                    glm::vec2(xCoord, zCoord));
    } else {
        answer = Math::barryCentric(glm::vec3(1, heights[xTile + 1][zTile], 0),
                                    glm::vec3(1, heights[xTile + 1][zTile + 1], 1),
                                    glm::vec3(0, heights[xTile][zTile + 1], 1),
                                    glm::vec2(xCoord, zCoord));
    }

    return answer;
}

glm::vec3 TerrainChunk::calculateNormal(int x, int y) {
    GLfloat heightL = getHeight(x-1, y);
    GLfloat heightR = getHeight(x+1, y);
    GLfloat heightD = getHeight(x, y-1);
    GLfloat heightU = getHeight(x, y+1);

    glm::vec3 normal(heightL-heightR, 2, heightD-heightU);
    normal = normalize(normal);
    return normal;
}