#pragma once

#include "Skybox.h"
#include "../core/AbstractRenderer.h"

class SkyboxRenderer: public AbstractRenderer {
public:
    Skybox* skybox;

public:
    SkyboxRenderer();

    void render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera);
};
