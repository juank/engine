#include "SkyboxRenderer.h"

SkyboxRenderer::SkyboxRenderer():
    AbstractRenderer{"../resources/shaders/skybox.vs", "../resources/shaders/skybox.fs"} {
}

void SkyboxRenderer::render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera) {
    //glDisable(GL_CULL_FACE)

    glDepthFunc(GL_LEQUAL);  // Change depth function so depth test passes when values are equal to depth buffer's content
    //  glCullFace(GL_BACK)

    // Activate shader
    shader.use();

    glm::mat4 skyboxViewMatrix = glm::mat4(glm::mat3(viewMatrix));	// Remove any translation component of the view matrix

    shader.bindUniform("viewMatrix", skyboxViewMatrix);
    shader.bindUniform("projectionMatrix", projectionMatrix);


    (*skybox).draw(shader);

    //glEnable(GL_CULL_FACE)
    glDepthFunc(GL_LESS); // Set depth function back to default
}