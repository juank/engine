#pragma once

#include "../core/Shader.h"

class Skybox {
public:
    GLuint vao;
    GLuint vbo;

    GLuint texCubemap;

    Skybox();
    ~Skybox();

    void draw(Shader& shader);
};