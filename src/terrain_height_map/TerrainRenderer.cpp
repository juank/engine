#include "TerrainRenderer.h"
#include <unordered_map>
#include "../core/AbstractRenderer.h"
#include "TexTerrain.h"
#include "Terrain.h"

TerrainRenderer::TerrainRenderer() : AbstractRenderer("../resources/shaders/terrain.vs", "../resources/shaders/terrain.fs"),
    texTerrains() {
}

void TerrainRenderer::render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera) {
    // Activate shader
    shader.use();

    bindCommon(viewMatrix, projectionMatrix, camera);

    for (auto element : texTerrains) {
        auto texTerrain = element.first;
        auto terrains = element.second;

        shader.bindTexture("baseTextureSampler", texTerrain.texs[0]);
        shader.bindTexture("redTextureSampler", texTerrain.texs[1]);
        shader.bindTexture("greenTextureSampler", texTerrain.texs[2]);
        shader.bindTexture("blueTextureSampler", texTerrain.texs[3]);
        shader.bindTexture("blendTextureSampler", texTerrain.blendTex);

        //shader.bindTexture("depthTextureSampler", Sun.texDepth);

        for(auto terrain : terrains) {
            // Draw container
            //std::cout << "bind vao:" << terrain.model.vao << std::endl;
            glBindVertexArray(terrain.model.vao);


            glm::mat4 modelMatrix;
            shader.bindUniform("modelMatrix", modelMatrix);
            glDrawElements(GL_TRIANGLES, terrain.model.count, GL_UNSIGNED_INT, nullptr);

            glBindVertexArray(0); // unbind vao
        }
    }
}

void TerrainRenderer::addTerrain(Terrain& terrain) {

    if(texTerrains.find(terrain.texTerrain) == texTerrains.end()) {
        texTerrains[terrain.texTerrain] = std::vector<Terrain>();
    }
    texTerrains[terrain.texTerrain].push_back(terrain);
}