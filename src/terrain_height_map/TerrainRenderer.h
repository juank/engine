#pragma once

class TerrainRenderer : public AbstractRenderer {
public:
    std::unordered_map<TexTerrain, std::vector<Terrain>> texTerrains;

    TerrainRenderer();

    void render(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera);
    void addTerrain(Terrain& terrain);
};
