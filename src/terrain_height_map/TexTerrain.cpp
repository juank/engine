#include "TexTerrain.h"
#include <vector>
#include <glad/glad.h>


TexTerrain::TexTerrain(std::vector<GLuint> texs, GLuint blendTex) :
        texs(texs),
        blendTex(blendTex) {
}

bool TexTerrain::operator==(const TexTerrain &rhs) const { return blendTex == rhs.blendTex; }

namespace std {
    template <>
    struct hash<TexTerrain>
    {
        std::size_t operator()(const TexTerrain& k) const
        {
            using std::hash;

            return hash<int>()(k.blendTex);
        }
    };
}