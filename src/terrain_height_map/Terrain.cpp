#include "Terrain.h"
#include <glad/glad.h>
#include "TexTerrain.h"
#include "../model/Model.h"
#include "../core/OpenGLLoader.h"
#include "../util/Math.h"
#include "../util/Image.h"

Terrain::Terrain(TexTerrain texTerrain, GLuint xTile, GLuint zTile, OpenGLLoader& loader, const char* heightMapName) :
    texTerrain{texTerrain},
    x{GLfloat(xTile) * SIZE},
    z{GLfloat(zTile) * SIZE},
    model{} {

    model = generateTerrain(loader, heightMapName);
}

SimpleMesh Terrain::generateTerrain(OpenGLLoader& loader, const char* heightMapName) {
    int width, height, nrChannels;
    //stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
    unsigned char* data = stbi_load(heightMapName, &width, &height, &nrChannels, 0);
    Image hightGenerator(data, width, height, nrChannels);

    return generateTerrain(loader, height, hightGenerator);
}

SimpleMesh Terrain::generateTerrain(OpenGLLoader& loader, GLint vertexCount, Image& hightGenerator) {
    heights = std::vector< std::vector<GLfloat> >(vertexCount, std::vector<GLfloat>(vertexCount, 0.0f));

    int count = vertexCount * vertexCount;
    auto vertices = std::vector<GLfloat>(count * 3, 0.0f);
    auto normals = std::vector<GLfloat>(count * 3, 0.0f);
    auto texCoords = std::vector<GLfloat>(count * 2, 0.0f);
    auto indices = std::vector<GLuint>(6 * (vertexCount-1) * (vertexCount-1), 0);
    int vertexPointer = 0;
    for( int i=0; i<vertexCount; i++ ) {
        for( int j=0; j<vertexCount; j++ ) {
            auto height = getHeight(j, i, hightGenerator);
            heights[j][i] = height;

            vertices[(vertexPointer*3)] = GLfloat(j)/(GLfloat(vertexCount) - 1) * GLfloat(SIZE);
            vertices[(vertexPointer*3)+1] = height;
            vertices[(vertexPointer*3)+2] = GLfloat(i)/(GLfloat(vertexCount) - 1) * GLfloat(SIZE);
            auto normal = calculateNormal(j, i, hightGenerator);
            normals[vertexPointer*3] = normal.x;
            normals[(vertexPointer*3)+1] = normal.y;
            normals[(vertexPointer*3)+2] = normal.z;
            texCoords[vertexPointer*2] = GLfloat(j)/GLfloat(vertexCount - 1);
            texCoords[(vertexPointer*2)+1] = GLfloat(i)/GLfloat(vertexCount - 1);

            /*std::cout << "v:" << vertexPointer << "-> " << vertices[(vertexPointer*3)] << ", " << vertices[(vertexPointer*3)+1]
                        << ", " << vertices[(vertexPointer*3)+2] << std::endl;*/
            /*std::cout << "uv:" << vertexPointer << "-> " << texCoords[(vertexPointer*2)] << ", " << texCoords[(vertexPointer*2)+1]
                       << std::endl;*/

            vertexPointer++;
        }
    }
    int pointer = 0;
    for( int gz=0; gz<vertexCount-1; gz++) {
        for( int gx=0; gx<vertexCount-1; gx++) {
            int topLeft = (gz*vertexCount)+gx;
            int topRight = topLeft + 1;
            int bottomLeft = ((gz+1)*vertexCount)+gx;
            int bottomRight = bottomLeft + 1;
            indices[pointer++] = GLuint(topLeft);
            indices[pointer++] = GLuint(bottomLeft);
            indices[pointer++] = GLuint(topRight);
            indices[pointer++] = GLuint(topRight);
            indices[pointer++] = GLuint(bottomLeft);
            indices[pointer++] = GLuint(bottomRight);
        }
    }
    return loader.loadVao(vertices, texCoords, normals, indices);
}

GLfloat Terrain::getHeight(GLfloat worldX, GLfloat worldZ) {
    GLfloat terrainX = worldX - x;
    GLfloat terrainZ = worldZ - z;
    GLfloat gridSquareSize = SIZE / GLfloat(heights.size() - 1);
    int xTile = int(floor(double(terrainX / gridSquareSize)));
    int zTile = int(floor(double(terrainZ / gridSquareSize)));
    if( xTile >= heights.size() - 1 || zTile >= heights.size() - 1 || xTile < 0 || zTile < 0 ) {
        return 0;
    }

    GLfloat xCoord = fmod(terrainX, gridSquareSize) / gridSquareSize;
    GLfloat zCoord = fmod(terrainZ, gridSquareSize) / gridSquareSize;

    GLfloat answer;
    if( xCoord <= (1-zCoord) ) {
        answer = Math::barryCentric(glm::vec3(0, heights[xTile][zTile], 0),
                                    glm::vec3(1, heights[xTile + 1][zTile], 0),
                                    glm::vec3(0, heights[xTile][zTile + 1], 1),
                                    glm::vec2(xCoord, zCoord));
    } else {
        answer = Math::barryCentric(glm::vec3(1, heights[xTile + 1][zTile], 0),
                                    glm::vec3(1, heights[xTile + 1][zTile + 1], 1),
                                    glm::vec3(0, heights[xTile][zTile + 1], 1),
                                    glm::vec2(xCoord, zCoord));
    }

    return answer;
}



glm::vec3 Terrain::calculateNormal(int x, int y, Image hightGenerator) {
    GLfloat heightL = getHeight(x-1, y, hightGenerator);
    GLfloat heightR = getHeight(x+1, y, hightGenerator);
    GLfloat heightD = getHeight(x, y-1, hightGenerator);
    GLfloat heightU = getHeight(x, y+1, hightGenerator);

    glm::vec3 normal(heightL-heightR, 2, heightD-heightU);
    normal = normalize(normal);
    return normal;
}

GLfloat Terrain::getHeight(int x, int y, Image hightGenerator) {
    if( x<=0 || x>=hightGenerator.width || y<=0 || y >= hightGenerator.height ) {
        return 0;
    }

    GLfloat height(hightGenerator.getRed(x, y));

    //std::cout << "h:" << height << std::endl;

    height -= 255/2;
    height /= 255/2;

    //height += MAX_PIXEL_COLOR/2
    //height /= MAX_PIXEL_COLOR/2
    height *= MAX_HEIGHT;

    //std::cout << "h:" << height << std::endl;

    return height;
}