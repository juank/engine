#pragma once

class TexTerrain {
public:
    std::vector<GLuint> texs;
    GLuint blendTex;

    TexTerrain(std::vector<GLuint> texs, GLuint blendTex);

    bool operator==(const TexTerrain &rhs) const { return blendTex == rhs.blendTex; }
};