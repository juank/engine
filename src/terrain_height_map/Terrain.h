#pragma once

class Terrain {
    GLfloat SIZE = 800;
    GLfloat MAX_HEIGHT = 40;
    GLfloat MAX_PIXEL_COLOR = 256 * 256 * 256;

    GLfloat x;
    GLfloat z;

    std::vector< std::vector<GLfloat > > heights;

public:
    TexTerrain texTerrain;
    SimpleMesh model;

    Terrain(TexTerrain texTerrain, GLuint xTile, GLuint zTile, OpenGLLoader& loader, const char* heightMapName);

    SimpleMesh generateTerrain(OpenGLLoader& loader, const char* heightMapName);
    SimpleMesh generateTerrain(OpenGLLoader& loader, GLint vertexCount, Image& hightGenerator);
    GLfloat getHeight(GLfloat worldX, GLfloat worldZ);
    glm::vec3 calculateNormal(int x, int y, Image hightGenerator);
    GLfloat getHeight(int x, int y, Image hightGenerator);
};