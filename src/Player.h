#pragma once

#include "model/ModelEntity.h"
#include "camera/Camera.h"
#include "terrain/Terrain.h"

class Player : public InputTarget {
public:
    ModelEntity* modelEntity;
    Camera* camera;
    Terrain* terrain;

    glm::vec3 worldUp;
    glm::vec3 front;
    glm::vec3 up;
    glm::vec3 right;

    // Eular Angles
    float yaw;
    float pitch;

    float cameraDistance;

    Player(ModelEntity* modelEntity, Camera* camera, Terrain* terrain);

    void processAction(InputAction action, float deltaTime) override;
    void processMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true) override;
    void processMouseScroll(float yoffset) override;

private:
    void updateVectors();
    void updateCamera();
};