#include "Player.h"

Player::Player(ModelEntity* modelEntity, Camera* camera, Terrain* terrain) :
    InputTarget{20.0f, 0.1f},
    modelEntity{modelEntity},
    camera{camera},
    terrain{terrain},
    worldUp{glm::vec3(0.0f, 1.0f, 0.0f)},
    yaw{90.0f},
    pitch{-89.0f},
    cameraDistance{200.0f} {

    updateVectors();
    updateCamera();
}

void Player::processAction(InputAction action, float deltaTime) {
    float velocity = movementSpeed * deltaTime;
    if (action == FORWARD)
        modelEntity->position += front * velocity;
    if (action == BACKWARD)
        modelEntity->position -= front * velocity;
    if (action == LEFT)
        modelEntity->position -= right * velocity;
    if (action == RIGHT)
        modelEntity->position += right * velocity;
    if (action == UP)
        modelEntity->position += up * velocity;
    if (action == DOWN)
        modelEntity->position -= up * velocity;

    terrain->update(*modelEntity);

    updateCamera();
}

void Player::processMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch) {
    if(false) {
        xoffset *= mouseSensitivity;
        yoffset *= mouseSensitivity;

        yaw += xoffset;

        modelEntity->yaw = glm::radians(90 - yaw);
        pitch += yoffset;

        // Make sure that when pitch is out of bounds, screen doesn't get flipped
        if (constrainPitch) {
            if (pitch > 89.0f)
                pitch = 89.0f;
            if (pitch < -89.0f)
                pitch = -89.0f;
        }

        // Update Front, Right and Up Vectors using the updated Eular angles
        updateVectors();
        updateCamera();
    }
}

void Player::processMouseScroll(float yoffset) {
    /*if (camera->Zoom >= 1.0f && camera->Zoom <= 45.0f)
        camera->Zoom -= yoffset;
    if (camera->Zoom <= 1.0f)
        camera->Zoom = 1.0f;
    if (camera->Zoom >= 45.0f)
        camera->Zoom = 45.0f;*/

    //std::cout << "scroll " << yoffset << std::endl;
    cameraDistance += -yoffset*10;

    updateVectors();
    updateCamera();
}

void Player::updateVectors() {
    // Calculate the new Front vector
    glm::vec3 front;
    front.x = cos(glm::radians(yaw)) /** cos(glm::radians(pitch))*/;
    //front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) /** cos(glm::radians(pitch))*/;
    this->front = glm::normalize(front);
    // Also re-calculate the Right and Up vector
    this->right = glm::normalize(glm::cross(front, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    this->up    = glm::normalize(glm::cross(right, front));
}

void Player::updateCamera() {
    //camera->position = modelEntity->position - (front *20.0f) + glm::vec3(0.0f, cameraHight, 0.0f);

    glm::vec3 cameraFront = this->front + glm::vec3(0.0f, sin(glm::radians(pitch)), 0.0f);
    cameraFront = glm::normalize(cameraFront);
    camera->position = modelEntity->position - (cameraFront * cameraDistance);
    camera->lookAt(modelEntity->position);
}