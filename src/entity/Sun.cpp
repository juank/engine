#include "Sun.h"
#include "../_lib/glad/glad.h"

glm::vec3 Sun::position = glm::vec3(1500.0, 1000.0, 0.0);
glm::vec3 Sun::direction = glm::normalize(glm::vec3(-1.0, -1.0, 0.0));
glm::vec3 Sun::color = glm::vec3(1, 1, 1);
GLfloat Sun::ambientStrength = 0.5;
glm::mat4 Sun::lightSpaceMatrix = glm::mat4();
GLuint Sun::texDepth = 0;
