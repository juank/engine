#pragma once


#include "../_lib/glad/glad.h"
//#include "../_lib/glm/detail/type_mat.hpp"
#include "../_lib/glm/vec3.hpp"
#include "../_lib/glm/detail/type_mat4x4.hpp"

class Sun {
public:
    static glm::vec3 position;
    static glm::vec3 direction;
    static glm::vec3 color;
    static GLfloat ambientStrength;

    static glm::mat4 lightSpaceMatrix;
    static GLuint texDepth;

private:
    Sun();
};
