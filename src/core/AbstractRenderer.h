#pragma once

#include "Shader.h"
#include "../camera/Camera.h"

class AbstractRenderer {
public:
    Shader shader;

    AbstractRenderer(const char* vertexFile, const char* fragmentFile);
    void bindCommon(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera);
};

