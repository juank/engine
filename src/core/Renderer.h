#pragma once

#include "../terrain/TerrainRenderer.h"
#include "../Display.h"
#include "../model/ModelRenderer.h"
#include "../skybox/SkyboxRenderer.h"
#include "../model/ModelInstanceRenderer.h"

class Renderer {
private:
    GLfloat FOV;
    GLfloat NEAR_PLANE;
    GLfloat FAR_PLANE;

    Display& display;
    SkyboxRenderer skyboxRenderer;
    TerrainRenderer terrainRenderer;
    ModelRenderer modelRenderer;
//    let staticRenderer: StaticRenderer
//    let lightRenderer: LightRenderer
    ModelInstanceRenderer modelInstanceRenderer;

/*    var optionLastChangeTime:GLfloat = 0;
    var optionPoligonLine = false

    var framebuffer = Framebuffer()
    var screenShader = Shader(vertexFile:"scene.vs", fragmentFile:"scene.fs")
    var screenModel = Screen()

    let depthFramebuffer = DepthFramebuffer()
    let depthShader = Shader(vertexFile:"depth.vs", fragmentFile:"depth.fs")
    let depthInstanceShader = Shader(vertexFile:"depth_instance.vs", fragmentFile:"depth.fs")*/

public:
    Renderer(Display& display, OpenGLLoader& openGlLoader);
    ~Renderer();

    void render(Camera& camera);
    //private func depthRender(_ camera: Camera);
    //func addStaticEntity(_ entity: Entity);
    void addModelEntity(ModelEntity* entity);
    void addTerrain(TerrainChunk& terrain);
    void removeTerrain(glm::vec2 position);
    void clearTerrains();
    void addModelInstace(glm::mat4& modelMatrix);
    void prepareModelInstace(Model& model);
    void setSkybox(Skybox& skybox);
    void loadAnimation(const std::string& path);
    void enablePolygonMode();
    void disablePolygonMode();
    void load();
};

