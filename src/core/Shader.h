#pragma once

#include "../_lib/glad/glad.h"
#include "../_lib/glm/glm.hpp"
#include <unordered_map>

class Shader {
private:
    struct SamplerLocation {
        GLint location;
        GLint texIndex;
        GLenum glTexture;

        SamplerLocation(){}

        SamplerLocation(GLint location, GLuint texIndex, GLenum glTexture) :
                location(location),
                texIndex(texIndex),
                glTexture(glTexture) {
        }
    };

    std::unordered_map<std::string, SamplerLocation> samplerTexLocations;
    std::unordered_map<std::string, SamplerLocation> samplerCubeTexLocations;
    GLuint availableTexIndex;

    std::string name;

public:
    unsigned int ID;

    Shader(const char* vertexPath, const char* fragmentPath, const char* geometryPath = nullptr);

    void use();
    void bindUniform(const std::string &name, bool value) const;
    void bindUniform(const std::string &name, int value) const;
    void bindUniform(const std::string &name, float value) const;
    void bindUniform(const std::string &name, const glm::vec2 &value) const;
    void bindUniform(const std::string &name, float x, float y) const;
    void bindUniform(const std::string &name, const glm::vec3 &value) const;
    void bindUniform(const std::string &name, float x, float y, float z) const;
    void bindUniform(const std::string &name, const glm::vec4 &value) const;
    void bindUniform(const std::string &name, float x, float y, float z, float w);
    void bindUniform(const std::string &name, const glm::mat2 &mat) const;
    void bindUniform(const std::string &name, const glm::mat3 &mat) const;
    void bindUniform(const std::string &name, const glm::mat4 &mat) const;
    void bindTexture(const std::string& uniform, GLint tex);
    void bindTextureCubeMap(const std::string& uniform, GLint tex);

private:
    SamplerLocation getSamplerLocation(const std::string& uniform, std::unordered_map<std::string, SamplerLocation>& samplerLocations);
    void checkCompileErrors(GLuint shader, std::string type);
};