#pragma once

#include "../_lib/glad/glad.h"
#include <tuple>

class Clock {
public:
    static GLfloat delta;  // Time between current frame and last frame
    static GLfloat last; // Time of last frame
    static GLfloat current; // Time of current frame

    static std::tuple<GLfloat, GLfloat> fps;

    static void frameStart();
};