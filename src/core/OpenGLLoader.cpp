#include "OpenGLLoader.h"
#include <iostream>
#include "../_lib/stb/stb_image.h"

OpenGLLoader::OpenGLLoader(){
}

OpenGLLoader::~OpenGLLoader() {
    for (auto vao = vaos.begin(); vao != vaos.end(); vao++) {
        glDeleteVertexArrays(1, &(*vao));
    }

    for (auto vbo = vaos.begin(); vbo != vaos.end(); vbo++) {
        glDeleteBuffers(1, &(*vbo));
    }

    for (auto texture = textures.begin(); texture != textures.end(); texture++) {
        glDeleteBuffers(1, &(*texture));
    }
}

SimpleMesh OpenGLLoader::loadVao(std::vector<GLfloat>& vertices, std::vector<GLfloat>& uvs, std::vector<GLuint>& indices) {
    static std::vector<GLfloat> emptyVector(0);
    return loadVao(vertices, uvs, emptyVector, indices);
}

SimpleMesh OpenGLLoader::loadVao(std::vector<GLfloat>& vertices, std::vector<GLfloat>& uvs, std::vector<GLfloat>& normals, std::vector<GLuint>& indices) {
    std::cout << "loadvao" << std::endl;
    GLuint vao, ebo, vboPosition, vboUvs, vboNormals;

    glGenVertexArrays(1, &vao);
    std::cout << "loadvao2" << std::endl;
    vaos.insert(vao);

    glGenBuffers(1, &ebo);
    vbos.insert(ebo);

    glGenBuffers(1, &vboPosition);
    vbos.insert(vboPosition);

    glGenBuffers(1, &vboUvs);
    vbos.insert(vboUvs);

    if (normals.size() > 0) {
        glGenBuffers(1, &vboNormals);
        vbos.insert(vboNormals);
    }

    glBindVertexArray(vao);

    // indices VBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*indices.size(), indices.data(), GL_STATIC_DRAW);

    GLsizei stride = 5;
    if (normals.size() > 0) {
        stride += 3;
    }
    stride *= sizeof (float) * 0;

    // position VBO
    glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, false, stride, (void*) 0);
    glEnableVertexAttribArray(0); // location = 0 on vertex shader

    // texture VBO
    glBindBuffer(GL_ARRAY_BUFFER, vboUvs);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * uvs.size(), uvs.data(), GL_STATIC_DRAW);

    // TexCoord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, false, stride, (void*) (0 * 3 * sizeof (float)));
    glEnableVertexAttribArray(1); // location = 1 on vertex shader

    if (normals.size() > 0) {
        // normal VBO
        glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * normals.size(), normals.data(), GL_STATIC_DRAW);

        // normal attribute
        glVertexAttribPointer(2, 3, GL_FLOAT, false, stride, (void*) (0 * 5 * sizeof (float)));
        glEnableVertexAttribArray(2); // location = 2 on vertex shader
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed,
    // the call to glVertexAttribPointer registered VBO as the currently bound
    // vertex buffer object so afterwards we can safely unbind.

    glBindVertexArray(0); // Unbind VAO; it's always a good thing to
    // unbind any buffer/array to prevent strange bugs.
    // remember: do NOT unbind the EBO, keep it bound to this VAO.

    std::cout << "bind vao:" << vao << " indices:" << indices.size() << std::endl;
    return SimpleMesh(vao, GLsizei(indices.size()));
}
    
GLuint OpenGLLoader::loadVao(std::vector<GLfloat> &vertices) {

    GLuint vao, vbo;

    glGenVertexArrays(1, &vao);
    vaos.insert(vao);

    glGenBuffers(1, &vbo);
    vbos.insert(vbo);

    glBindVertexArray(vao);

    GLsizei stride = 5 * sizeof (float);

    // position VBO
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, false, stride, (void*) 0);
    glEnableVertexAttribArray(0); // location = 0 on vertex shader

    // TexCoord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, false, stride, (void*) (3 * sizeof (float)));
    glEnableVertexAttribArray(1); // location = 1 on vertex shader

    glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed,
    // the call to glVertexAttribPointer registered VBO as the currently bound
    // vertex buffer object so afterwards we can safely unbind.

    glBindVertexArray(0); // Unbind VAO; it's always a good thing to
    // unbind any buffer/array to prevent strange bugs.
    // remember: do NOT unbind the EBO, keep it bound to this VAO.

    return vao;
}

GLuint OpenGLLoader::loadTexture(const std::string& texFileName) {
  return loadTexture(texFileName, GL_REPEAT);
}

GLuint OpenGLLoader::loadTexture(const std::string& texFileName, GLint wrapping) {
    if (pathTex.find(texFileName) != pathTex.end()) {
        return pathTex[texFileName];
    }

    // Load and create textures
    GLuint tex;

    // Globally change loader to 0,0 in lower left
    //SGLImageLoader.flipVertical = true

    // == Texture 1
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex); // All upcoming GL_TEXTURE_2D operations
    // now have effect on this texture object
    // Set texture wrapping to GL_REPEAT (usually basic wrapping method) GL_REPEAT GL_MIRRORED_REPEAT GL_CLAMP_TO_EDGE GL_CLAMP_TO_BORDER
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapping);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapping);

    // Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // GL_LINEAR GL_NEAREST GL_NEAREST_MIPMAP_NEAREST
    // GL_LINEAR_MIPMAP_NEAREST GL_NEAREST_MIPMAP_LINEAR GL_LINEAR_MIPMAP_LINEAR
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // GL_NEAREST GL_LINEAR


    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
    unsigned char *data = stbi_load(texFileName.c_str(), &width, &height, &nrChannels, 0);
    if (data) {
        GLenum format;
        if (nrChannels == 1)
            format = GL_RED;
        else if (nrChannels == 3)
            format = GL_RGB;
        else if (nrChannels == 4)
            format = GL_RGBA;

        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 0); // unbind texture
    } else {
        std::cout << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);

    pathTex[texFileName] = tex;

    std::cout << "loaded texture: " << texFileName << " tex:" << tex << std::endl;
    return tex;
}

GLuint OpenGLLoader::loadCubemapTexture(const std::string& directory) {
    GLuint tex;
    glGenTextures(1, &tex);
    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_CUBE_MAP, tex);

    char* fileNames[] = {(char*)"right.png", (char*)"left.png", (char*)"top.png", (char*)"bottom.png", (char*)"back.png", (char*)"front.png"};
    for(int i=0; i<6; i++) {

        int width, height, nrChannels;
        stbi_set_flip_vertically_on_load(false); // tell stb_image.h to flip loaded texture's on the y-axis.
        std::string fileName = directory + fileNames[i];
        std::cout << "cubemap:" << fileName << std::endl;

        unsigned char *data = stbi_load(fileName.c_str(), &width, &height, &nrChannels, 0);
        if (data) {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, 0); // unbind texture


        } else {
            std::cout << "Failed to load texture" << std::endl;
        }
        stbi_image_free(data);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return tex;
}



