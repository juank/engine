#include "AbstractRenderer.h"
#include "../entity/Sun.h"

AbstractRenderer::AbstractRenderer(const char* vertexFile, const char* fragmentFile) :
    shader{vertexFile, fragmentFile} {
}

/*void AbstractRenderer::bindCommon(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera) {
    //shader.bindUniform("lightSpaceMatrix", lightSpaceMatrix);
    bindCommon(viewMatrix, projectionMatrix, camera);
}*/

void AbstractRenderer::bindCommon(glm::mat4& viewMatrix, glm::mat4& projectionMatrix, Camera& camera) {
    shader.bindUniform("viewMatrix", viewMatrix);
    shader.bindUniform("projectionMatrix", projectionMatrix);

    // directional
    shader.bindUniform("directionalLight.direction", Sun::direction);
    shader.bindUniform("directionalLight.ambient", Sun::color * Sun::ambientStrength);
    shader.bindUniform("directionalLight.diffuse", Sun::color * 0.5f);
    shader.bindUniform("directionalLight.specular", Sun::color);

    /*
    // point lights
    for (index, light) in LightRenderer.pointLights.enumerate() {
        shader.bindUniform("pointLights[\(index)].position", vec: light.position)
        shader.bindUniform("pointLights[\(index)].ambient", vec: light.ambient)
        shader.bindUniform("pointLights[\(index)].diffuse", vec: light.diffuse)
        shader.bindUniform("pointLights[\(index)].specular", vec: light.specular)
        shader.bindUniform("pointLights[\(index)].constant", float: light.constant)
        shader.bindUniform("pointLights[\(index)].linear", float: light.linear)
        shader.bindUniform("pointLights[\(index)].quadratic", float: light.quadratic)
    }

    for light in LightRenderer.spotLights {
        shader.bindUniform("spotLight.position", vec: camera.position-vec3(0.0, 1.0, 0.0))
        shader.bindUniform("spotLight.ambient", vec: camera.flashLight.color * 0.2)
        shader.bindUniform("spotLight.diffuse", vec: camera.flashLight.color * 0.5)
        shader.bindUniform("spotLight.specular", vec: camera.flashLight.color)
        shader.bindUniform("spotLight.constant", float: light.constant)
        shader.bindUniform("spotLight.linear", float: light.linear)
        shader.bindUniform("spotLight.quadratic", float: light.quadratic)
        shader.bindUniform("spotLight.direction", vec: camera.cameraFront)
        shader.bindUniform("spotLight.cutOff", float: light.cutOff)
        shader.bindUniform("spotLight.outerCutOff", float: light.outerCutOff)
    }*/

    //shader.bindUniform("viewPosition", camera.position);
}