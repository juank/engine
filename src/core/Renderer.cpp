#include "Renderer.h"
#include "../_lib/glm/gtc/matrix_transform.hpp"

Renderer::Renderer(Display& display, OpenGLLoader& openGLLoader) :
    display{display},
    FOV{45.0f},
    NEAR_PLANE{0.1},
    FAR_PLANE{10000},
    terrainRenderer{openGLLoader},
    modelRenderer{},
    skyboxRenderer{},
    modelInstanceRenderer{} {

    // Setup OpenGL options
    glEnable(GL_DEPTH_TEST);
    //glDepthFunc(GL_ALWAYS)

    //jkglEnable(GL_STENCIL_TEST);
    //jkglStencilFunc(GL_NOTEQUAL, 1, 0xFF);
    //jkglStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    //jkglStencilMask(0x00); // Make sure we don't update the stencil buffer while drawing the floor

    //jkglEnable(GL_BLEND);
    //jkglBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //jkglEnable(GL_CULL_FACE);
    //jkglCullFace(GL_BACK);
    //glFrontFace(GL_CW);

    /*modelRenderer = ModelRenderer()
    staticRenderer = StaticRenderer()

    lightRenderer = LightRenderer()*/

    //quadVAO.setTexture(framebuffer.texColor)
    //quadVAO.setTexture("terrain_grass.png")
}

Renderer::~Renderer() {
}

void Renderer::render(Camera& camera) {


    //depthRender(camera)

    //framebuffer.use()

    // Clear the colorbuffer
    glClearColor(0.2, 0.3, 0.3, 1.0);
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    //glStencilMask(0xFF);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    //glStencilMask(0x00);
    // Create transformations
    auto viewMatrix = camera.GetViewMatrix();

    auto aspectRatio = GLfloat(display.width) / GLfloat(display.height);
    glm::mat4 projectionMatrix = glm::perspective(glm::radians(camera.Zoom), aspectRatio, NEAR_PLANE, FAR_PLANE);

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    modelInstanceRenderer.render(viewMatrix, projectionMatrix, camera);
    terrainRenderer.render(viewMatrix, projectionMatrix, camera);

    modelRenderer.render(viewMatrix, projectionMatrix, camera);
    skyboxRenderer.render(viewMatrix, projectionMatrix, camera);

    /*
    modelInstanceRenderer.render(&viewMatrix, projectionMatrix: &projectionMatrix, camera: camera)
    terrainRenderer.render(viewMatrix, projectionMatrix, camera);
    lightRenderer.render(&viewMatrix, projectionMatrix: &projectionMatrix, camera: camera)
    modelRenderer.render(&viewMatrix, projectionMatrix: &projectionMatrix, camera: camera)
    skyboxRenderer.render(&viewMatrix, projectionMatrix: &projectionMatrix, camera: camera)*/

    /////////////////////////////////////////////////////
    // Bind to default framebuffer again and draw the
    // quad plane with attched screen texture.
    // //////////////////////////////////////////////////
   /* glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // Clear all relevant buffers
    glClearColor(1.0, 1.0, 1.0, 1.0); // Set clear color to white (not really necessery actually, since we won't be able to see behind the quad anyways)
    //glClear(GL_COLOR_BUFFER_BIT);
    glStencilMask(0xFF);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT)
    glStencilMask(0x00);
    glDisable(GL_DEPTH_TEST); // We don't care about depth information when rendering a single quad
    glDisable(GL_STENCIL_TEST);

    // Draw Screen
    screenShader.use();
    glBindVertexArray(screenModel.meshes[0].VAO);
    //screenShader.bindTexture("texture_diffuse1", tex: depthFramebuffer.texDepth);
    screenShader.bindTexture("texture_diffuse1", tex: framebuffer.texColor);	// Use the color attachment texture as the texture of the quad plane
    glDrawElements(GL_TRIANGLES, GLsizei(screenModel.meshes[0].indices.count), GL_UNSIGNED_INT, nil)
    glBindVertexArray(0);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_STENCIL_TEST);*/
}

/*func depthRender(_ camera: Camera) {
    glViewport(0, 0, depthFramebuffer.SHADOW_WIDTH, depthFramebuffer.SHADOW_HEIGHT)
    depthFramebuffer.use()
    glClear(GL_DEPTH_BUFFER_BIT)
    //glDisable(GL_CULL_FACE)

    let nearPlane:GLfloat = 1.0
    let farPlane:GLfloat = 2000
    var lightProjection:mat4 = SGLMath.ortho(-100.0, 100.0, -100.0, 100.0, nearPlane, farPlane)

    var lookAt = vec3( camera.position.x, 0.0, camera.position.z)
    var lookFrom = lookAt - (Sun.direction * 1000)
    var lightView: mat4 = SGLMath.lookAt(lookFrom, lookAt, vec3( 0.0, 1.0, 0.0))

    var lightSpaceMatrix: mat4 = lightProjection * lightView

    glCullFace(GL_FRONT);

    depthInstanceShader.use()
    depthInstanceShader.bindUniform("lightSpaceMatrix", matrix: &lightSpaceMatrix)
    modelInstanceRenderer.depthRender()

    depthShader.use()
    depthShader.bindUniform("lightSpaceMatrix", matrix: &lightSpaceMatrix)
    modelRenderer.depthRender(depthShader)

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glViewport(0, 0, WIDTH, HEIGHT)
    glCullFace(GL_BACK);
    //glEnable(GL_CULL_FACE)
    Sun.lightSpaceMatrix = lightSpaceMatrix
    Sun.texDepth = depthFramebuffer.texDepth
}
*/

void Renderer::addModelEntity(ModelEntity* entity) {
    modelRenderer.addEntity(entity);
}

void Renderer::addTerrain(TerrainChunk& terrain) {
    terrainRenderer.addTerrain(terrain);
}

void Renderer::removeTerrain(glm::vec2 position) {
    terrainRenderer.removeTerrain(position);
}

void Renderer::clearTerrains() {
    terrainRenderer.clear();
}

void Renderer::addModelInstace(glm::mat4& modelMatrix) {
    modelInstanceRenderer.addInstance(modelMatrix);
}

void Renderer::prepareModelInstace(Model& model) {
    modelInstanceRenderer.prepareModel(model);
}

void Renderer::setSkybox(Skybox& skybox) {
    skyboxRenderer.skybox = &skybox; //TODO move
}

void Renderer::loadAnimation(const std::string& path) {
    modelRenderer.loadAnimation(path);
}

void Renderer::enablePolygonMode() {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void Renderer::disablePolygonMode() {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Renderer::load() {
    terrainRenderer.load();
}