#include "Clock.h"
#include <iostream>
#include <GLFW/glfw3.h>

void Clock::frameStart() {
    // Calculate deltatime of current frame
    current = GLfloat(glfwGetTime());
    delta = current - last;
    last = current;

    std::get<0>(fps) += 1.0f;
    std::get<1>(fps) += delta;
    if(std::get<1>(fps) >= 5) {
        std::cout << "FPS: " << (std::get<0>(fps))/(std::get<1>(fps)) << std::endl;
        std::get<0>(fps) = 0.0f;
        std::get<1>(fps) = 0.001f;
    }
}

GLfloat Clock::delta = 0;
GLfloat Clock::last = 0;
GLfloat Clock::current = 0;
std::tuple<GLfloat, GLfloat> Clock::fps = std::make_tuple(0.0f, 0.001f);