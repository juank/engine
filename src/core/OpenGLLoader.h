#pragma once

#include <unordered_set>
#include <unordered_map>
#include <string>
#include <vector>
#include "../model/SimpleMesh.h"

class OpenGLLoader {
private:
    std::unordered_set<GLuint> vaos;
    std::unordered_set<GLuint> vbos;
    std::unordered_set<GLuint> textures;
    std::unordered_map<std::string, GLuint> pathTex;

public:
    OpenGLLoader();
    ~OpenGLLoader();

    SimpleMesh loadVao(std::vector<GLfloat>& vertices, std::vector<GLfloat>& uvs, std::vector<GLuint>& indices);
    SimpleMesh loadVao(std::vector<GLfloat>& vertices, std::vector<GLfloat>& uvs, std::vector<GLfloat>& normals, std::vector<GLuint>& indices);
    GLuint loadVao(std::vector<GLfloat> &vertices);
    GLuint loadTexture(const std::string& texFileName);
    GLuint loadTexture(const std::string& texFileName, GLint wrapping);
    GLuint loadCubemapTexture(const std::string& directory);
};


