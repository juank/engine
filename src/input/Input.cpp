#include "Input.h"
#include "../core/Clock.h"

bool firstMouse = true;
float lastX, lastY;

static InputTarget* target = nullptr;

Input::Input(Display& display, InputTarget* targetPtr) {
    this->window = display.window;
    target = targetPtr;

    lastX = display.width / 2.0f;
    lastY = display.height / 2.0f;

    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    // tell GLFW to capture our mouse

    //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Input::pollEvents() {
    glfwPollEvents();
}

void Input::processInput() {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        target->processAction(FORWARD, Clock::delta);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        target->processAction(BACKWARD, Clock::delta);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        target->processAction(LEFT, Clock::delta);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        target->processAction(RIGHT, Clock::delta);
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        target->processAction(UP, Clock::delta);
    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
        target->processAction(DOWN, Clock::delta);

    if (glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS) {
        cursorDisabled = !cursorDisabled;
        if(cursorDisabled) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        } else {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
    }
}

void Input::framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

void Input::mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    if (firstMouse) {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    target->processMouseMovement(xoffset, yoffset);
}

void Input::scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    target->processMouseScroll(yoffset);
}