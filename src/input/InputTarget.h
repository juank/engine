#pragma once

#include "../_lib/glad/glad.h"
#include "InputAction.h"

class InputTarget {
public:
    float movementSpeed;
    float mouseSensitivity;

    InputTarget(float movementSpeed, float mouseSensitivity);

    virtual void processAction(InputAction action, GLfloat delta) = 0;
    virtual void processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true) = 0;
    virtual void processMouseScroll(float yoffset) = 0;
};