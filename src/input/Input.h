#pragma once

#include "../_lib/glad/glad.h"
#include <GLFW/glfw3.h>
#include "../Display.h"
#include "InputTarget.h"


class Input {
public:
    GLFWwindow* window;
    bool cursorDisabled = true;

    Input(Display& display, InputTarget* targetPtr);

    void pollEvents();
    void processInput();
    static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
    static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
    static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
};


