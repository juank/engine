#pragma once

#include "_lib/glad/glad.h"
#include <GLFW/glfw3.h>

class Display {
public:
    GLFWwindow* window;
    unsigned int width;
    unsigned int height;

    Display(unsigned int width, unsigned int height);
    ~Display();

    bool shouldClose();
    void swapBuffers();
};