#include "Camera.h"
#include "../_lib/glm/gtc/matrix_transform.hpp"

Camera::Camera(glm::vec3 position, glm::vec3 up, float yaw, float pitch) :
    InputTarget{SPEED, SENSITIVTY},
    Zoom{ZOOM},
    position{position},
    WorldUp{up},
    Yaw{yaw},
    Pitch{pitch},
    Front{glm::vec3(0.0f, 0.0f, -1.0f)} {

    updateCameraVectors();
}

Camera::Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) :
    InputTarget{SPEED, SENSITIVTY},
    Front{glm::vec3(0.0f, 0.0f, -1.0f)},
    Zoom{ZOOM} {

    position = glm::vec3(posX, posY, posZ);
    WorldUp = glm::vec3(upX, upY, upZ);
    Yaw = yaw;
    Pitch = pitch;
    updateCameraVectors();
}

glm::mat4 Camera::GetViewMatrix() {
    return glm::lookAt(position, position + Front, Up);
}

void Camera::processAction(InputAction action, float deltaTime) {
    float velocity = movementSpeed * deltaTime;
    if (action == FORWARD)
        position += Front * velocity;
    if (action == BACKWARD)
        position -= Front * velocity;
    if (action == LEFT)
        position -= Right * velocity;
    if (action == RIGHT)
        position += Right * velocity;
    if (action == UP)
        position += Up * velocity;
    if (action == DOWN)
        position -= Up * velocity;
}

void Camera::processMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch) {
    xoffset *= mouseSensitivity;
    yoffset *= mouseSensitivity;

    Yaw   += xoffset;
    Pitch += yoffset;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch)
    {
        if (Pitch > 89.0f)
            Pitch = 89.0f;
        if (Pitch < -89.0f)
            Pitch = -89.0f;
    }

    // Update Front, Right and Up Vectors using the updated Eular angles
    updateCameraVectors();
}

void Camera::processMouseScroll(float yoffset) {
    if (Zoom >= 1.0f && Zoom <= 45.0f)
        Zoom -= yoffset;
    if (Zoom <= 1.0f)
        Zoom = 1.0f;
    if (Zoom >= 45.0f)
        Zoom = 45.0f;
}

void Camera::lookAt(glm::vec3 target) {
    Front = glm::normalize( target - position );
}

void Camera::updateCameraVectors() {
    // Calculate the new Front vector
    glm::vec3 front;
    front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
    front.y = sin(glm::radians(Pitch));
    front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
    Front = glm::normalize(front);
    // Also re-calculate the Right and Up vector
    Right = glm::normalize(glm::cross(Front, WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    Up    = glm::normalize(glm::cross(Right, Front));
}