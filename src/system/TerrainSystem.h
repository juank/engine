#pragma once

#include "../Player.h"

class TerrainSystem {
    Terrain* terrain;
    Player* player;

    bool running;
public:
    TerrainSystem(Terrain& terrain, Player& player);

    void run();
    void stop();
};