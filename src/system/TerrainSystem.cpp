#include <thread>
#include "TerrainSystem.h"
#include <iostream>

TerrainSystem::TerrainSystem(Terrain &terrain, Player &player) :
    terrain{&terrain},
    player{&player},
    running{false} {
}

void TerrainSystem::run() {
    this->running = true;

    while(this->running) {

        terrain->updateChunks(player->modelEntity->position);
        std::cout << "run thread terrain " << std::this_thread::get_id() << " " << this << std::endl;
        std::this_thread::sleep_for (std::chrono::seconds(5));
    }
}

void TerrainSystem::stop() {
    std::cout << "stop thread terrain " << std::this_thread::get_id() << " " << this << std::endl;
    this->running = false;
}
